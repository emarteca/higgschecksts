Require Export List.
Require Export Arith.
Require Export String.
Open Scope string_scope.
Require Import Max.
Require Import Le.
Require Import Coq.Program.Equality.
Require Export Ascii.
Require Peano_dec.
Require FunInd.

Import ListNotations.

Inductive id : Type :=
  | Id : string -> id.

Definition beq_id x y :=
  match x,y with
    | Id n1, Id n2 => if string_dec n1 n2 then true else false
  end.

(* Types *) 

Inductive primType : Type :=
  | prim_Number : primType
  | prim_String : primType
  | prim_Bool   : primType
  | prim_Void   : primType.

(* Thinking of using numbers as names, just to simplify checking for equality *)
Inductive methName : Type :=
  | Method : id -> methName.

Inductive interName : Type :=
  | Interface : id -> interName.

Inductive className : Type :=
  | Class : id -> className.

Inductive fieldName : Type :=
  | Field : id -> fieldName.

Inductive fieldDecl : Type := 
  | fDecl : list (fieldName * ty) -> fieldDecl

with methDecl : Type := 
  | mDecl : list (methName * methType) -> methDecl (* probably gonna have to prove that these types are all methTypes *)

with methType : Type :=
  | methType_ty : list ty -> ty -> methType

with ty : Type :=
  | prim_dTyp      : primType -> ty
  | mf_dTyp        : methDecl -> fieldDecl -> ty
  | Any            : ty
  | inter_dTyp     : interName -> (*methDecl ->*) ty
  | class_dTyp     : className -> ty
  (*| class_structTyp: className -> methDecl -> fieldDecl -> ty *)
 (* | methT_Typ      : methType -> ty*).

Inductive lit : Type :=                    (* HMMM *)
  | BoolLit  : bool -> lit
  | numLit   : nat -> lit  (* this might have to be real or something later *)
  | StrLit   : string -> lit
  | UndefLit : lit
  | DieLit   : lit.

Inductive location : Type :=
  | thisLoc : nat (* id *) -> location
  | nulLoc  : location.

(* Before I had value here, but now I'm going to use value as stat -> Prop of whether or not it's
   a value (i.e. if the exp is a loc or a literal + type pair, I think *)

Inductive var : Type :=
  | Var : id -> var.

Definition beq_var (v1 v2 : var) : bool :=
  match v1 with
    | Var n1 => match v2 with
                | Var n2 => beq_id n1 n2
                end
end.

Inductive interDefn : Type :=
  | baseInter : interDefn
  | interBody : interName -> list interDefn -> methDecl -> fieldDecl -> interDefn.

Inductive methDefn : Type := mDefn : list (methName * methType) -> list methDef_conts -> methDefn (* lists are parallel -- do we need check here? *)

with fieldDefn : Type := fDefn : list (fieldName * ty) -> list exp (*list fieldName -> list fieldDef_conts*) -> fieldDefn (* lists are parallel *)

with classDefn : Type :=
  | baseClass      : classDefn 
  | classBody : className -> classDefn -> list interDefn -> methDefn -> fieldDecl -> classDefn

with val : Type :=
  | v_locc      : location -> val (* shouldnt be able to have a location before runtime! -- still true, but now  we need it for typing *)
  | v_lit_prim : lit -> primType -> val

with exp : Type := 
  | e_value      : val -> exp
 (* | e_loc        : location -> exp
  | e_lit_prim   : lit -> primType -> exp*)
  | e_var        : var -> exp            (* x .... WAT ----- imma assume it's a var *)
  | e_FTDefns    : methDefn -> fieldDefn -> exp
  | e_theDefns   : methDefn -> fieldDefn -> exp
  | e_newClass   : className -> list exp -> exp
  | e_methCall   : exp -> methName -> list exp -> exp
  | e_accessCall : exp -> exp -> list exp -> exp
  | e_fieldAcc   : exp -> fieldName -> exp
  | e_fieldAcc_TR: exp -> fieldName -> exp
  | e_access     : exp -> exp -> exp
  (*| e_typeE      : ty -> exp -> exp
  | e_RT         : list typeOrExp -> exp*)
  | e_cast       : ty -> exp -> exp
  | e_FTcast     : ty -> exp -> exp
  | e_contAssign : t_contractAssign -> exp
  | e_checkCont  : t_checkContract -> exp
  | e_bobj       : location -> list methDefn -> fieldDefn -> list ty -> exp
  
with t_contractAssign : Type :=
  | t_CA : exp -> ty -> t_contractAssign
  
with t_checkContract : Type :=
  | t_CC : exp -> id -> t_checkContract

with typeOrExp : Type :=
  | typeTE : ty -> typeOrExp
  | expTE  : exp -> typeOrExp


with stat : Type :=
  | s_exp         : exp -> stat
  | s_skip        : stat
  | s_stats       : stat -> stat -> stat
  | s_varAssign   : var -> ty -> exp -> stat
  | s_assignNoVar : var -> exp -> stat
  | s_fieldAssign : exp -> fieldName -> exp -> stat
  | s_accAssign   : exp -> exp -> exp -> stat
  | s_ifElse      : exp -> stat -> stat -> stat
  | s_ret         : exp -> stat
 
(*with fieldDef_conts : Type :=
  | fD_conts : ty -> exp -> fieldDef_conts*)

with methDef_conts : Type := mD_conts : list (var * ty) -> ty -> stat -> exp -> methDef_conts.
 
 
 
(* helper functions for getting stuff *) 
 
Definition beq_loc (l1 l2: location): bool :=
  match l1 with
  | nulLoc => match l2 with 
              | nulLoc => true
              | _ => false 
              end
  | thisLoc n1 => match l2 with 
                  | nulLoc => false
                  | thisLoc n2 => (beq_nat (* beq_id *) n1 n2)
                  end
  end.

Fixpoint beq_str (s1 s2 : string) : bool :=
  match s1 with
  | EmptyString => match s2 with
                   | EmptyString => true
                   | _ => false
                   end
  | String a s1' => match s2 with
                    | EmptyString => false
                    | String b s2' => match ascii_dec a b with
                                      | left _ => beq_str s1' s2'
                                      | right _ => false
                                      end
                    end
  end.

Definition beq_fName (f1 f2: fieldName) : bool := 
  match f1 with
  | Field fn1 => match f2 with 
                 | Field fn2 => (beq_id fn1 fn2)
                 end
  end.

Definition beq_cName (c1 c2: className) : bool := 
  match c1 with
  | Class cn1 => match c2 with
                 | Class cn2 => (beq_id cn1 cn2)
                 end
  end.
  
Definition beq_iName (i1 i2: interName) : bool :=
  match i1 with
  | Interface in1 => match i2 with
                 | Interface in2 => (beq_id in1 in2)
                 end
  end.
  
Definition beq_mName (m1 m2: methName) : bool := 
  match m1 with
  | Method mn1 => match m2 with
                  | Method mn2 => (beq_id mn1 mn2)
                  end
  end.
  
(*Definition beq_fDecl (f1 f2: fieldDecl) : bool := *)
  
(*Fixpoint stLength {A : Type} (l : list A) : nat :=
  match l with
  | nil => 0
  | h::t => 1 + (stLength t)
end.*)

Definition stLength {A : Type} (l : list A) : nat :=
  Datatypes.length l.

Fixpoint stSnoc {A : Type} (l : list A) (x : A) : list A :=
  match l with
  | nil => x::nil
  | h::t => h::(stSnoc t x)
end.


 
 
 
  
Definition typeEnv : Type := list (var * ty).
(*Definition locStore : Type := list (var * location).*)
  
(* -------------------------- runtime grammar (almost the same, but with no types, and the potential for aux fcts *)

(* for the contracts *)
Inductive prim_shape : Type :=  
  | primitive : primType -> prim_shape
  | function :  methName -> prim_shape
  | class :     className -> prim_shape
  | structS :    prim_shape. (* does this have to be more specific? we probably dont need the types? *)
  
Inductive beta_obls : Type :=
  | prim_obl      : prim_shape -> beta_obls
  | field_obl     : fieldName -> ty -> beta_obls
  | memberFct_obl : methName -> methType (* this has to be a methtype *) -> beta_obls
  | fctArg_obl    : ty -> beta_obls
  | ret_obl       : ty -> beta_obls.
  
Inductive beta_contracts : Type :=
  | empty_cont  : beta_contracts (* for Any *)
  | lobls_cont  : list beta_obls -> beta_contracts.
  
  
  
(* runtime grammar that's not contract stuff (contracts are included in the beta_objRep) *)  

(*Inductive beta_fieldDecl : Type := 
  | beta_fDecl : list fieldName -> beta_fieldDecl.

Inductive beta_methDecl : Type := 
  | beta_mDecl : list methName -> beta_methDecl (* probably gonna have to prove that these types are all methTypes *).

Inductive beta_interDefn : Type :=
  | beta_baseInter : beta_interDefn
  | beta_interBody : interName -> list beta_interDefn -> beta_methDecl -> beta_fieldDecl -> beta_interDefn.
*)
Inductive beta_methDefn : Type := beta_mDefn : list (methName * methType) -> list beta_methDef_conts -> beta_methDefn (* lists are parallel -- do we need check here? *)

with beta_fieldDefn : Type := beta_fDefn : list (fieldName * ty) -> list beta_exp (*list fieldName -> list fieldDef_conts*) -> beta_fieldDefn (* lists are parallel *)

with beta_classDefn : Type :=
  | beta_baseClass      : beta_classDefn 
  | beta_classBody : className -> beta_classDefn -> list interDefn -> beta_methDefn -> fieldDecl -> beta_classDefn

with beta_val : Type :=
  | beta_v_loc      : location -> beta_val
  | beta_v_lit_prim : lit -> primType -> beta_val
  (*| beta_v_runObj   : beta_runtimeObj -> beta_val *)
  | beta_v_ERROR    : beta_val

with beta_exp : Type := 
  | beta_e_value      : beta_val -> beta_exp
  | beta_e_obj        : beta_objRep -> beta_exp
  (*| e_loc        : location -> exp
  | e_lit_prim   : lit -> primType -> exp*)
  | beta_e_var        : var -> beta_exp            (* x .... WAT ----- imma assume it's a var *)
  | beta_e_theDefns   : beta_methDefn -> beta_fieldDefn -> beta_exp
  | beta_e_defFirstTR : beta_methDefn -> beta_fieldDefn -> beta_exp
  | beta_e_newClass   : className -> list beta_exp -> beta_exp
  | beta_e_methCall   : beta_exp -> methName -> list beta_exp -> beta_exp
  | beta_e_accessCall : beta_exp -> beta_exp -> list beta_exp -> beta_exp
  | beta_e_fieldAcc   : beta_exp -> fieldName -> beta_exp
  | beta_e_access     : beta_exp -> beta_exp -> beta_exp
  (*| beta_e_RT         : list typeOrExp -> exp*)
  | beta_e_contAssign : contractAssign -> beta_exp
  | beta_e_checkCont  : checkContract -> beta_exp
  | beta_e_cast       : ty -> beta_exp -> beta_exp
  | beta_e_FTcast     : ty -> beta_exp -> beta_exp
  
with contractAssign : Type :=
  | CA : beta_exp -> ty -> contractAssign
  
with checkContract : Type :=
  | CC_meth : beta_exp -> methName -> checkContract
  | CC_field : beta_exp -> fieldName -> checkContract
(*
with typeOrExp : Type :=
  | typeTE : ty -> typeOrExp
  | expTE  : exp -> typeOrExp*)


with beta_stat : Type :=
  | beta_s_error       : beta_stat
  | beta_s_exp         : beta_exp -> beta_stat
  | beta_s_skip        : beta_stat
  | beta_s_stats       : beta_stat -> beta_stat -> beta_stat
  | beta_s_varAssign   : var -> ty -> beta_exp -> beta_stat
  | beta_s_assignNoVar : var -> beta_exp -> beta_stat
  | beta_s_fieldAssign : beta_exp -> fieldName -> beta_exp -> beta_stat
  | beta_s_accAssign   : beta_exp -> beta_exp -> beta_exp -> beta_stat
  | beta_s_ifElse      : beta_exp -> beta_stat -> beta_stat -> beta_stat
  | beta_s_ret         : beta_exp -> beta_stat
 
(*with fieldDef_conts : Type :=
  | fD_conts : ty -> exp -> fieldDef_conts*)

with beta_methDef_conts : Type := beta_mD_conts : list (var * ty) -> ty -> beta_stat -> beta_exp -> beta_methDef_conts

with beta_objRep : Type :=
  | nulObj : beta_objRep
  (*| bobj_fDecl   : location -> list methDecl -> option fieldDecl -> list beta_contracts -> beta_objRep*)
  | bobj_fDefn   : location -> list beta_methDefn -> beta_fieldDefn -> list beta_contracts -> beta_objRep
  
with beta_runtimeObj : Type := 
  | nulrobj : beta_runtimeObj
  | brobj_fDefn : location -> list beta_methDefn -> beta_valFieldDefn -> list beta_contracts -> beta_runtimeObj

with beta_valFieldDefn : Type :=
  | beta_vfDefn : list (fieldName * ty) -> list beta_val -> beta_valFieldDefn.
  
Fixpoint CA_list (lbes : list beta_exp) (lts : list ty) : list beta_exp :=
  match lbes, lts with
  | [], _ => []
  | _, [] => []
  | be :: Lbs, t :: Lts => (beta_e_contAssign (CA be t)) :: (CA_list Lbs Lts)
  end.
Hint Unfold CA_list.
  
Inductive bheap_contents : Type :=
  | bheap_cont_obj  : beta_runtimeObj -> bheap_contents
  | bheap_cont_prim : lit -> primType -> bheap_contents
  (*| bheap_cont_ERR : beta_v_ERROR -> bheap_contents*).

(*Definition beta_heap : Type := list (location * bheap_contents).*)
Definition beta_heap : Type := list bheap_contents. 

Definition beta_localStore : Type := list (var * location). 

(*Inductive beta_localStore : Type :=  (* list (var * location) *)
  | emptyStore  : beta_localStore
  | addVarStore : beta_localStore -> var -> location -> beta_localStore
  | doubleStore : beta_localStore -> beta_localStore -> beta_localStore.*)

(* maybe can ignore the stack ??? TODO *)
Inductive beta_stack : Type :=
  | bstack_null : beta_stack
  | bstack_more : beta_stack -> beta_localStore (*-> beta_evalContext*) ->  beta_stack.  
  
(* make it more like a tree so we have scoping *)
Inductive signature : Type :=
  | sig_null     : signature
  | sig_interDef : signature -> interDefn -> signature
  | sig_classDef : signature -> beta_classDefn -> signature
  (*| sig_sigs     : signature -> signature -> signature*).
  

Inductive beta_state : Type :=
  | bstate_state : signature -> beta_heap -> beta_stack -> beta_localStore -> beta_state.  
    
  
  
(* helper functions *)  
(* searching the current scope: when there's the S1 S2 option, search the 2nd one first *)
Fixpoint getClass (S : signature) (Cn : className) : option beta_classDefn :=
  match S with
    | sig_null => None
    | sig_interDef S1 Idef => (getClass S1 Cn)
    | sig_classDef S1 Cdef => match Cdef with 
                           | beta_baseClass => (getClass S1 Cn)
                           | beta_classBody Cn1 Cdef1 I1s Ms Fs => match (beq_cName Cn Cn1) with 
                                                                  | true => Some Cdef
                                                                  | _ => (getClass S1 Cn)
                                                                  end
                           end
    (*| sig_sigs S1 S2 => match (getClass S2 Cn) with 
                        | None => (getClass S1 Cn)
                        | Some Cd => Some Cd  
                        end*)
  end.
  
Fixpoint getInterface (S : signature) (Inm : interName) : option interDefn :=
  match S with
    | sig_null => None
    | sig_classDef S1 Cdef => None
    | sig_interDef S1 Idef => match Idef with 
                           | baseInter => (getInterface S1 Inm)
                           | interBody In1 I1s Ms Fs => match (beq_iName Inm In1) with 
                                                              | true => Some Idef
                                                              | _ => (getInterface S1 Inm)
                                                              end
                           end
    (*| sig_sigs S1 S2 => match (getInterface S2 Inm) with 
                        | None => (getInterface S1 Inm)
                        | Some Idef => Some Idef  
                        end*)
  end.

Definition getFields (Cd: option beta_classDefn) : option fieldDecl := 
  match Cd with 
  | None => None
  | Some beta_baseClass => None
  | Some (beta_classBody Cn1 Cd1 I1s Md1 Fd1) => Some Fd1
  end.
  
Fixpoint getFields_hier (Cd: beta_classDefn) : list fieldDecl :=
  match Cd with
  | beta_baseClass => []
  | beta_classBody Cn1 Cd1 I1s Md1 Fd1 => Fd1 :: (getFields_hier Cd1)
  end.

Definition getMethods (Cd: option beta_classDefn) : option beta_methDefn :=
  match Cd with 
  | None => None
  | Some beta_baseClass => None
  | Some (beta_classBody Cn1 Cd1 I1s Md1 Fd1) => Some Md1 
  end. 
  
Fixpoint getMethods_hier (Cd: beta_classDefn) : list beta_methDefn :=
  match Cd with 
  | beta_baseClass => []
  | beta_classBody Cn1 Cd1 I1s Md1 Fd1 => Md1 :: (getMethods_hier Cd1) 
  end.

Definition getMethodList (md: option beta_methDefn) : list beta_methDefn :=
  match md with
  | None => []
  | Some md1 => [md1]
  end.
  
Definition getMethDeclFromDefn (mD: option beta_methDefn) : methDecl :=
  match mD with
  | None => mDecl []
  | Some (beta_mDefn lmnts lmcs) => mDecl lmnts (* (fst (split lmnts)) (snd (split lmnts))*)
  end.

Fixpoint getMethDeclListFromDefnList (mDl: list beta_methDefn) : list methDecl :=
  match mDl with
  | [] => []
  | mD :: L => (getMethDeclFromDefn (Some mD)) :: (getMethDeclListFromDefnList L)
  end.
  
Fixpoint getMethNameListFromMethDeclList (mdcl : list methDecl) : list methName :=
  match mdcl with
  | [] => []
  | (mDecl mnts) :: L => (fst (split mnts)) ++ (getMethNameListFromMethDeclList L)
  end.
  
Fixpoint getMethTypeListFromMethDeclList (mdcl : list methDecl) : list methType :=
  match mdcl with
  | [] => []
  | (mDecl mnts) :: L => (snd (split mnts)) ++ (getMethTypeListFromMethDeclList L)
  end.
  
Fixpoint getMNTListFromMethDeclList (mdcl: list methDecl) : list (methName * methType) :=
  match mdcl with
  | [] => []
  | (mDecl lmnts) :: L => lmnts ++ (getMNTListFromMethDeclList L)
  end.
  
Definition getMethDeclFromMethDeclList (mdcl: list methDecl) : methDecl :=
  (*mDecl (getMethNameListFromMethDeclList mdcl) (getMethTypeListFromMethDeclList mdcl). *)
  mDecl (getMNTListFromMethDeclList mdcl).
  
Definition getMethDeclFromMethDefnList (mDs: list beta_methDefn) : methDecl :=
  getMethDeclFromMethDeclList (getMethDeclListFromDefnList mDs).

Definition getMethNameFromDefn (mD : option beta_methDefn) : list methName :=
  match mD with
  | None => []
  | Some (beta_mDefn lmnts lmcs) => (fst (split lmnts))
  end.
  
Definition getMethTypeFromDefn (mD : option beta_methDefn) : list methType :=
  match mD with
  | None => []
  | Some (beta_mDefn lmnts lmcs) => (snd (split lmnts))
  end.
  
Definition getFieldDeclFromDefn (fD: option beta_fieldDefn) : fieldDecl :=
  match fD with
  | None => fDecl [] 
  | Some (beta_fDefn lfnts lfcs) => fDecl lfnts (*(fst (split lfnts)) (snd (split lfnts))*)
  end. 
  
Definition getFieldDeclFromOptionDecl (fD: option fieldDecl) : fieldDecl :=
  match fD with
  | None => fDecl []
  | Some f => f
  end.

Fixpoint getFieldNameListFromFieldDeclList (fDs: list fieldDecl) : list fieldName :=
  match fDs with
  | [] => []
  | (fDecl fnts) :: L => (fst (split fnts)) ++ (getFieldNameListFromFieldDeclList L)
  end.
  
Fixpoint getFieldTypeListFromFieldDeclList (fDs: list fieldDecl) : list ty :=
  match fDs with
  | [] => []
  | (fDecl fnts) :: L => (snd (split fnts)) ++ (getFieldTypeListFromFieldDeclList L)
  end.

Fixpoint getFNTListFromFieldDeclList (fDs: list fieldDecl) : list (fieldName * ty) :=
  match fDs with
  | [] => []
  | (fDecl fnts) :: L => fnts ++ (getFNTListFromFieldDeclList L)
  end.

Definition getFieldDeclFromFieldDeclList (fDs: list fieldDecl) : fieldDecl :=
  (*fDecl (getFieldNameListFromFieldDeclList fDs) (getFieldTypeListFromFieldDeclList fDs).*)
  fDecl (getFNTListFromFieldDeclList fDs).   
  
  
  

(*Definition store_typing : Type := list (location * ty). *)
Definition store_typing : Type := list ty.  
  
  
  
Inductive program : Type :=
  | prog : signature -> stat -> program.

Inductive typContext : Type :=
  | typConx : signature -> store_typing (* -> beta_tagHeap *)-> typeEnv -> typContext.

Inductive primValue : beta_stat -> Prop :=
  | p_prim : forall p pt, primValue (beta_s_exp (beta_e_value (beta_v_lit_prim p pt)))
  | p_loc  : forall l, primValue (beta_s_exp (beta_e_value (beta_v_loc l))).

Inductive value: beta_stat -> Prop :=
  | v_prim : forall p pt, value (beta_s_exp (beta_e_value (beta_v_lit_prim p pt)))
  | v_loc  : forall l, value (beta_s_exp (beta_e_value (beta_v_loc l)))
  (*| v_doneObj : forall o, value (beta_s_exp (beta_e_value (beta_v_runObj o)))*).
  
Inductive objIsValue : beta_objRep -> Prop :=
  | v_nullObj : objIsValue nulObj
  | v_obj  : forall l mnds fns fes cs,
                    (expListAllValues fes) ->
                    objIsValue (bobj_fDefn l mnds (beta_fDefn fns fes) cs)
with expListAllValues: list beta_exp -> Prop :=
  | e_empty : expListAllValues []
  | e_cons  : forall e les,
              value (beta_s_exp e) ->
              expListAllValues les ->
              expListAllValues (e :: les).  
              
Inductive error: beta_stat -> Prop :=
  | s_err : error (beta_s_error).

Hint Constructors value objIsValue expListAllValues primValue.

(* Testing equality *)

(* Gonna need to be able to test equality for types, and by extension various other inductives 
   I wonder if we could just do subtyping test instead of equality, since <= includes ==
   But it might be nice to also have an equality check *)



(* Substitution *)

(* Here I have a feeling this will be implemented following the logic of the evaluation context 
   But maybe not!  So far just using the approach used in FWJ. 

   Thought of maybe using heap locations instead of vars, but if that ends up making sense
   then it should be easy enough to change later *)

(* Copying notation from FWJ here, it might be useful to be able to substitute many 
   vars at once into an expression *)
Notation SubstRel := (list (var * beta_exp)).

Fixpoint get (x : var) (E : SubstRel) : option beta_exp:=
  match E with
  | nil => None
  | (x',e)::E' => if (beq_var x x') then (Some e) else (get x E')
end.

Fixpoint remOne (v : var) (E : SubstRel) : SubstRel :=
 match E with
 | (va, e)::L => match (beq_var v va) with
                   | true => (remOne v L)
                   | false => (va, e)::(remOne v L)
                 end
 | [] => []
 end.

Fixpoint remLocs (vts : list (var * ty)) (E: SubstRel) : SubstRel :=
  match vts with
  | (v, t) :: L => remLocs L (remOne v E)
  | [] => E
  end.

Fixpoint subst_exp (E : SubstRel) (e : beta_exp) : beta_exp :=
  match e with 
  (* first 2 basically make up e_value *) 
  (*| e_loc l => e
  | e_lit_prim l p => e*)
  | beta_e_value v => e
  | beta_e_obj (nulObj) => e
  | beta_e_obj (bobj_fDefn l mds (beta_fDefn fns fes) cs) => (*match (value (beta_s_exp e)) with
                                                             | True => e
                                                             | _ => *)(beta_e_obj (bobj_fDefn l mds (beta_fDefn fns (List.map (subst_exp E) fes)) cs))
                                                             (*end*)
  | beta_e_var v   => match get v E with
                      | None    => beta_e_var v
                      | Some e0 => e0
                      end
  | beta_e_theDefns (beta_mDefn mD mDc) (beta_fDefn fNTs fEs) => beta_e_theDefns (beta_mDefn mD (List.map (subst_md E) mDc)) (beta_fDefn fNTs (List.map (subst_exp E) fEs)) (*e_theDefns (mDefn (subst_md E mD)) (fDefn (subst_fd E fD))*)
  | beta_e_defFirstTR (beta_mDefn mD mDc) (beta_fDefn fNTs fEs) => beta_e_defFirstTR (beta_mDefn mD (List.map (subst_md E) mDc)) (beta_fDefn fNTs (List.map (subst_exp E) fEs)) (*e_theDefns (mDefn (subst_md E mD)) (fDefn (subst_fd E fD))*)
  | beta_e_newClass cn es => beta_e_newClass cn (List.map (subst_exp E) es)
  | beta_e_methCall e0 mn es => beta_e_methCall (subst_exp E e0) mn (List.map (subst_exp E) es)
  | beta_e_accessCall e0 e1 es => beta_e_accessCall (subst_exp E e0) (subst_exp E e1) (List.map (subst_exp E) es)
  | beta_e_fieldAcc e0 fn => beta_e_fieldAcc (subst_exp E e0) fn
  | beta_e_access e0 e1 => beta_e_access (subst_exp E e0) (subst_exp E e1)
  (*| e_typeE ty e0 => e_typeE ty (subst_exp E e0)
  | e_RT tes => e_RT (List.map (subst_typeOrExp E) tes) *)
  | beta_e_contAssign (CA e0 t0) => beta_e_contAssign (CA (subst_exp E e0) t0)
  | beta_e_checkCont (CC_meth e0 s0) => beta_e_checkCont (CC_meth (subst_exp E e0) s0)
  | beta_e_checkCont (CC_field e0 s0) => beta_e_checkCont (CC_field (subst_exp E e0) s0) 
  | beta_e_cast t e => beta_e_cast t (subst_exp E e)
  | beta_e_FTcast t e => beta_e_FTcast t (subst_exp E e)
  end
(* with subst_typeOrExp (E : SubstRel) (te : typeOrExp) : typeOrExp :=
  match te with
  | typeTE t => te
  | expTE e => expTE (subst_exp E e) 
  end*)
 (*with subst_fd (E : SubstRel) (fD : (fieldName * ty * exp)) : (fieldName * ty * exp) :=
  match fD with
  | (fN, t, e) => (fN, t, (subst_exp E e))
  end*)
  (*match fD with
  | fD_conts t e => fD_conts t (subst_exp E e)
  end*)
 with subst_md (E : SubstRel) (mD : beta_methDef_conts) : beta_methDef_conts :=
  match mD with
  | beta_mD_conts vts t s e => beta_mD_conts vts t (subst_stat (remLocs vts E) s) (subst_exp (remLocs vts E) e)
 end
 with subst_stat (E : SubstRel) (s : beta_stat) : beta_stat :=
  match s with
  | beta_s_error => beta_s_error
  | beta_s_exp e => beta_s_exp (subst_exp E e)
  | beta_s_skip => s
  | beta_s_stats s1 s2 => beta_s_stats (subst_stat E s1) (subst_stat E s2)
  | beta_s_varAssign v t e => beta_s_varAssign v t (subst_exp E e) (* should I do anything with var, like add it to E? *)
  | beta_s_assignNoVar v e => beta_s_assignNoVar v (subst_exp E e)
  | beta_s_fieldAssign e1 fn e2 => beta_s_fieldAssign (subst_exp E e1) fn (subst_exp E e2) 
  | beta_s_accAssign e1 e2 e3 => beta_s_accAssign (subst_exp E e1) (subst_exp E e2) (subst_exp E e3)
  | beta_s_ifElse e1 s1 s2 => beta_s_ifElse (subst_exp E e1) (subst_stat E s1) (subst_stat E s2) 
  | beta_s_ret e => beta_s_ret (subst_exp E e)
 end.
 

(* The beta grammar -- i.e. the runtime configuration syntax.  Need this for the reduction rules, that use States 
   Not needed for subst *)
(* removed beta_stat b/c just put ret into the other stat
   the awkward part is, can you tell apart s_ret from s_exp???
Inductive beta_stat : Type :=  (* HMM i guess this ellipses means it's either stat from before or ret e?? *)
  | bstat_oldstat : stat -> beta_stat
  | bstat_ret     : exp -> beta_stat.*)

(*Inductive beta_evalContext : Type :=
  | bevalC_empty       : beta_evalContext
  | bevalC_Es          : beta_evalContext -> stat -> beta_evalContext
  | bevalC_var         : var -> beta_evalContext -> beta_evalContext
  | bevalC_varAssign   : var -> beta_evalContext -> beta_evalContext
  | bevalC_fieldAss    : beta_evalContext -> fieldName -> exp -> beta_evalContext
  | bevalC_valFieldAss : val -> fieldName -> beta_evalContext -> beta_evalContext
  | bevalC_locAss      : beta_evalContext -> exp -> exp -> beta_evalContext
  | bevalC_valLocAss   : val -> beta_evalContext -> exp -> beta_evalContext
  | bevalC_valValAss   : val -> val -> beta_evalContext -> beta_evalContext
  | bevalC_return      : beta_evalContext -> beta_evalContext
  | bevalC_ifElse      : beta_evalContext -> stat -> stat -> beta_evalContext
  | bevalC_methList    : methDefn -> list (fieldName * val) -> fieldName -> beta_evalContext -> list (fieldName * val) -> beta_evalContext
  | bevalC_newC        : list val -> beta_evalContext -> list exp -> beta_evalContext
  | bevalC_Emeth       : beta_evalContext -> methName -> list exp -> beta_evalContext
  | bevalC_vmeth       : val -> methName -> list val -> beta_evalContext -> list exp -> beta_evalContext
  | bevalC_EaccessCall : beta_evalContext -> exp -> list exp -> beta_evalContext
  | bevalC_vaccessCall : val -> beta_evalContext -> list exp -> beta_evalContext
  | bevalC_vAccvCall   : val -> val -> list val -> beta_evalContext -> list exp -> beta_evalContext
  | bevalC_EfieldAcc   : beta_evalContext -> fieldName -> beta_evalContext
  | bevalC_Eaccess     : beta_evalContext -> exp -> beta_evalContext
  | bevalC_vaccess     : val -> beta_evalContext.*)

(* New to the grammar: obligations (contracts are just lists of obligations) *)
(*Inductive beta_obls : Type := 

(*  | nominal_obl : className -> list methDecl -> list fieldDecl -> beta_obls
  | struct_obl  : list methDecl -> list fieldDecl -> beta_obls
  | funct_obl   : list ((list ty) * ty) -> beta_obls
  | prim_obl    : primType -> beta_obls.*)
  | nominal_obl : className -> beta_obls
(*  | meth_obl    : methDecl -> beta_obls*)
  | field_obl   : fieldName -> ty -> beta_obls
  | funct_obl   : methType -> beta_obls
  | meth_obl    : (methName * methType) -> beta_obls
  | prim_obl    : primType -> beta_obls
.*)

(*
Inductive ty_TCP : Type :=
  | prim_dTyp_TCP  : primType -> ty_TCP
  | mf_dTyp_TCP    : methDecl_TCP -> fieldDecl_TCP -> ty_TCP
  | Any_TCP        : ty_TCP
  | inter_dTyp_TCP : interName -> ty_TCP
  | class_dTyp_TCP : className -> ty_TCP
with fieldDecl_TCP : Type := 
  | fDecl_TCP : list fieldName -> list ty_TCP -> fieldDecl_TCP
with methDecl_TCP : Type := 
  | mDecl_TCP : list (methName * methType) -> methDecl_TCP
.*)



(* Helpers for the reduction rules *)

(*beta_localStore :
  | emptyStore  : beta_localStore
  | addVarStore : beta_localStore -> var -> location -> beta_localStore
  | doubleStore : beta_localStore -> beta_localStore -> beta_localStore. *)
  
(*Fixpoint locFromVarInStore (L : beta_localStore) (x : var) : location :=
  match L with 
  | emptyStore => nulLoc
  | addVarStore L1 x1 l1 => match (beq_var x1 x) with
                            | true => l1
                            | false => (locFromVarInStore L1 x)
                            end 
  | doubleStore L1 L2 => match (locFromVarInStore L2 x) with  
                         | nulLoc => (locFromVarInStore L1 x)
                         | thisLoc n => thisLoc n
                         end
  end.*)
Fixpoint locFromVarInStore (L : beta_localStore) (x : var) : location :=
  match L with 
  | [] => nulLoc
  | (x1, l1) :: rL => match (beq_var x1 x) with
                            | true => l1
                            | false => (locFromVarInStore rL x)
                            end 
  end.

(*Fixpoint findObjInHeap (H: beta_heap) (l: location) : beta_runtimeObj :=
  match H with
  | [] => nulrobj
  | (l1, (bheap_cont_obj O1))::H1 => match (beq_loc l1 l) with 
                             | true => O1
                             | false => (findObjInHeap H1 l)
                             end 
  | (l1, (bheap_cont_prim p1 t1))::H1 => (findObjInHeap H1 l)
  end.*)
Definition findObjInHeap (H : beta_heap) (l : location) : beta_runtimeObj :=
  match l with
  | nulLoc => nulrobj
  | thisLoc n => match (nth n H (bheap_cont_obj nulrobj)) with 
                 | (bheap_cont_obj O1) => O1
                 | (bheap_cont_prim p1 t1) => nulrobj
                 end
  end.
  
(*Fixpoint findHeapContsAtLoc (H : beta_heap) (l: location) : bheap_contents :=
  match H with
  | [] => (bheap_cont_obj nulrobj)
  | (l1, conts)::H1 => match (beq_loc l1 l) with 
                             | true => conts
                             | false => (findHeapContsAtLoc H1 l)
                             end
  end.  *)
Definition findHeapContsAtLoc (H : beta_heap) (l : location) : bheap_contents := 
  match l with
  | nulLoc => (bheap_cont_obj nulrobj)
  | thisLoc n => (nth n H (bheap_cont_obj nulrobj)) 
  end.

(* should never remove from the heap, the heap should only grow *)
(* maybe can do on-heap replacement *)
(*Fixpoint removeAtLocInHeap (H: beta_heap) (l: location) : beta_heap :=
  match H with
  | [] => []
  | (l1, O1) :: H1 => match (beq_loc l1 l) with 
                       | true => H1
                       | false => (l1, O1) :: (removeAtLocInHeap H1 l)
                       end 
  end.
*)
(* First, remove old fieldDef, then add new one *)
Fixpoint removeFieldDef (F: list ((fieldName * ty) * beta_val)) (f: fieldName) (v: beta_val) : list ((fieldName * ty) * beta_val) :=
   match F with
  | (((f1, t1), e1) :: F2) => match (beq_fName f f1) with
                                    | true => F2
                                    | false => (((f1, t1), e1) :: (removeFieldDef F2 f v))
                                    end (*(fDefn ((f1, (fD_conts t1 e1))::[])) ++ *)
  | [] => []
  end. 

(* shouldnt need this, use contracts for this TODO *)
Fixpoint typeLookupField (F: list ((fieldName * ty) * beta_val)) (f: fieldName) : ty :=
  match F with
  | (((f1, t1), e1) :: F2) => match (beq_fName f f1) with
                                    | true => t1
                                    | false => (typeLookupField F2 f)
                                    end (*(fDefn ((f1, (fD_conts t1 e1))::[])) ++ *)
  | [] => (prim_dTyp (prim_Void))
  end. 

(* HIDEOUS *)
Definition updateFieldDefnWithVal (F: (list ((fieldName * ty) * beta_val))) (f: fieldName) (v: beta_val) : beta_valFieldDefn :=
  beta_vfDefn (fst (split (((f, (typeLookupField F f)), v) :: (removeFieldDef F f v))))
             (snd (split (((f, (typeLookupField F f)), v) :: (removeFieldDef F f v)))).

(*Definition updateObjFieldWithVal (H: beta_heap) (f: fieldName) (l: location) (v: beta_val) : beta_heap :=
  match (findObjInHeap H l) with
  | nulrobj => H
  (*| (bobj_fDecl l1 M1 None K) => H
  | (bobj_fDecl l1 M1 (Some (fDecl FNT1)) K) => H (* dont update with val if declaration (only for defn) *) (* (l, (bobj_fDecl l1 M1 (updateFieldDeclWithVal F1 f v) K)) :: (removeAtLocInHeap H l) *)*)
  | (brobj_fDefn l1 M1 (beta_vfDefn FN1 FDC1) K) => (l, (bheap_cont_obj (brobj_fDefn l1 M1 (updateFieldDefnWithVal (combine FN1 FDC1) f v) K))) :: (removeAtLocInHeap H l)
  end. *)
  
(*Fixpoint updateObjFieldWithVal (H : beta_heap) (f : fieldName) (l : location) (v : beta_val) : beta_heap :=
  match H with
  | [] => []
  | (l1, (bheap_cont_prim lp pt)) :: rH => updateObjFieldWithVal rH f l v
  | (l1, (bheap_cont_obj nulrobj)) :: rH => updateObjFieldWithVal rH f l v
  | (l1, (bheap_cont_obj (brobj_fDefn l2 M1 (beta_vfDefn FN1 FDC1) K))) :: rH => match (beq_loc l1 l) with
                                        | false =>  updateObjFieldWithVal rH f l v
                                        | true => (l, (bheap_cont_obj (brobj_fDefn l1 M1 (updateFieldDefnWithVal (combine FN1 FDC1) f v) K))) :: rH
                                        end 
  end.*)
Fixpoint updateObjFieldWithVal (H: beta_heap) (f: fieldName) (l : location) (v: beta_val) (n: nat) : beta_heap :=
  match H with
  | [] => []
  | bhc :: rH => match l with
                 | nulLoc => bhc :: rH
                 | thisLoc n1 => match (beq_nat n1 n) with
                                 | false => bhc :: (updateObjFieldWithVal rH f l v (S n))
                                 | true => match bhc with
                                           | (bheap_cont_obj (brobj_fDefn l M1 (beta_vfDefn FN1 FDC1) K)) => (bheap_cont_obj (brobj_fDefn l M1 (updateFieldDefnWithVal (combine FN1 FDC1) f v) K)) :: rH
                                           | _ => bhc :: rH 
                                           end 
                                 end 
                 end
  end.

(* probably dont need this TODO *)
Definition toString (cv : lit) : string := (* toString only makes sense for literals that are strings right *)
  match cv with 
  | StrLit s => s
  | _        => EmptyString
  end.

Definition toBool (v : beta_val) : bool :=
  match v with
  | beta_v_lit_prim cv t => match cv with 
                    | BoolLit b => b
                    | _ => false
                    end
  | _ => false 
  end.

(* ok this is obviously a placeholder .... but what should it do O .o *)
(*Definition evalWithStat (E: beta_evalContext) (C: beta_state) (s: stat) : (beta_state * stat) :=
  (C, s).

*)

(* type equality is decidable -- hopefully won't need to describe HOW types are equal *)
Parameter type_eq_dec: forall (t1 t2: ty), {t1 = t2} + {t1 <> t2}.
(*Parameter ptype_eq_dec: forall (t1 t2: processed_ty), {t1 = t2} + {t1 <> t2}.*)
Parameter fDecl_eq_dec: forall (f1 f2: fieldDecl), {f1 = f2} + {f1 <> f2}.
Parameter mDecl_eq_dec: forall (m1 m2: methDecl), {m1 = m2} + {m1 <> m2}.
Parameter fName_eq_dec: forall (f1 f2: fieldName), {f1 = f2} + {f1 <> f2}.
Parameter fName_type_eq_dec: forall (fnt1 fnt2: (fieldName * ty)), 
                                {fnt1 = fnt2} + {fnt1 <> fnt2}.
Parameter mType_eq_dec: forall (mt1 mt2: methType), {mt1 = mt2} + {mt1 <> mt2}.
Parameter mName_mType_eq_dec: forall (mnt1 mnt2: (methName * methType)), 
                                {mnt1 = mnt2} + {mnt1 <> mnt2}.   
        
Fixpoint lSize (lfnts: list (fieldName * ty)): nat :=
  match lfnts with
  | [] => 0
  | fnt :: L => 1 + (lSize L)
  end.    

(*   
Fixpoint countNum (l1 : list nat) (l2: list nat) : nat :=
  match l1, l2 with
  | [], k => 0
  | l1 :: rL, k => 1 + (countNum rL k)
  end.

Fixpoint countNum (l : (list nat) * (list nat)) : nat :=
  match l with
  | ([], k) => 0
  | (l1 :: rL, k) => 1 + (countNum (rL, k))
  end.    *)
 (* 
Inductive hackType: Type :=
  | tyTy : ty -> hackType
  | fieldTy : fieldDecl -> hackType.
*)
(*Fixpoint processTypeForContract (t : ty) : ty_TCP :=
  match t with
  | Any => Any_TCP
  | prim_dTyp p => prim_dTyp_TCP p
  | inter_dTyp Inm => inter_dTyp_TCP Inm
  | class_dTyp Cnm => class_dTyp_TCP Cnm
  | mf_dTyp (mDecl mCs) (fDecl fDs) => mf_dTyp_TCP (mDecl_TCP mCs) (fDecl_TCP (fst (split fDs)) (List.map processTypeForContract (snd (split fDs))))
  end.*)
  
(*Inductive prim_shape : Type :=  
  | primitive : primType -> prim_shape
  | function :  prim_shape
  | class :     className -> prim_shape
  | structS :    prim_shape. (* does this have to be more specific? we probably dont need the types? *)

(* gonna have to put restrictions on the prim_shape in funct and obj obls *)
Inductive beta_obls : Type := 
  | empty_obl : beta_obls
  | ptrToContractWithType : ty -> beta_obls
  | prim_obl : prim_shape -> beta_obls
  | funct_obl : prim_shape -> list beta_obls (* for the args *) -> beta_obls (* for the return *) -> beta_obls
  | obj_obl : prim_shape ->  list (fieldName * beta_obls) -> list (methName * beta_obls) -> beta_obls.*)

(* method to get the structural type from a class, from the signature *)
(* should we have err_obl? then clean up later (for ex. if the nominal type referenced doesnt exist in context) *)

(*Fixpoint getMethDeclFromDefn (mns : list methName) (mdcs : list methDef_conts) : list (methName * ty) :=
  match mns, mdcs with
  | [], _ => []
  | _, [] => []
  | mn1 :: mn1s, (mD_conts lvts t1 s1 e1) :: mdc1s => (mn1, (methT_Typ (snd (split lvts)) t1)) :: (getMethDeclFromDefn mn1s mdc1s)
  end.*)

Definition getStructTypeForClass (cN: className) (S: signature) : ty :=
  mf_dTyp (getMethDeclFromDefn (getMethods (getClass S cN))) (getFieldDeclFromOptionDecl (getFields (getClass S cN))). 
  
(*Fixpoint toContract (S: signature) (seen: list ty) (t: ty) {struct t}: beta_obls :=
  match (in_dec type_eq_dec t seen) with
  | right _ =>
                match t with
                | Any => empty_obl
                | prim_dTyp p => prim_obl (primitive p)
                | mf_dTyp (mDecl lmns lmts) (fDecl lfns lfts) => obj_obl structS (combine lfns ((List.map (toContract S (t :: seen))) lfts)) (combine lmns ((List.map (toContract S (t :: seen))) lmts)) (*(toContract_methDecl S lmnts)*)
                | inter_dTyp iN (mDecl lmns lmts) => empty_obl (*obj_obl (interface iN) [] (combine lmns ((List.map (toContract S (t :: seen))) lmts)) *)
                | class_structTyp cN (mDecl lmns lmts) (fDecl lfns lfts) => obj_obl (class cN) (combine lfns ((List.map (toContract S (t :: seen))) lfts)) (combine lmns ((List.map (toContract S (t :: seen))) lmts))
                | methT_Typ lts retT => funct_obl function (List.map (toContract S (t :: seen)) lts) (toContract S (t :: seen) retT)
                end
  | left _ => ptrToContractWithType t 
  end.*)
  
Fixpoint getBOblsMethNameTypeList (lmnts: list (methName * methType)) : list beta_obls :=
  match lmnts with
  | [] => []
  | (mn, mt) :: L => (memberFct_obl mn mt) :: (getBOblsMethNameTypeList L)
  end.
  
Definition getBOblsMethDecl (mD: methDecl) : list beta_obls :=
  match mD with
  | mDecl lmnts => getBOblsMethNameTypeList lmnts
  end.
  
Fixpoint getBOblsFieldNameTypeList (lmnts: list (fieldName * ty)) : list beta_obls :=
  match lmnts with
  | [] => []
  | (fn, ft) :: L => (field_obl fn ft) :: (getBOblsFieldNameTypeList L)
  end.
  
Definition getBOblsFieldDecl (fD: fieldDecl) : list beta_obls :=
  match fD with
  | fDecl lfnts => getBOblsFieldNameTypeList lfnts
  end.
  
Definition toContract (S: signature) (t: ty): beta_contracts :=
  match t with
  | Any => empty_cont
  | prim_dTyp p => lobls_cont [prim_obl (primitive p)]
  | mf_dTyp mD fD => lobls_cont ( [(prim_obl structS)] ++ ((getBOblsMethDecl mD) ++ (getBOblsFieldDecl fD)))
  | inter_dTyp iN => empty_cont (* what should it be here?? *)
  | class_dTyp cN => lobls_cont ( [(prim_obl (class cN))] ++ ((getBOblsMethDecl (getMethDeclFromDefn (getMethods (getClass S cN)))) ++ (getBOblsFieldDecl (getFieldDeclFromOptionDecl (getFields (getClass S cN))))))
  end.
  
Definition toContract_optType (S: signature) (t: option ty): beta_contracts :=
  match t with
  | None => empty_cont
  | Some t => toContract S t
  end.
  
Fixpoint applyListOfContractsFromTypeList (S : signature) (lt : list ty) (be : beta_exp) : beta_exp :=
  match lt with
  | [] => be
  | t :: rlt => (beta_e_contAssign (CA (applyListOfContractsFromTypeList S rlt be) t))
  end.
  

(* Fixpoint applyListOfMethContractsFromMethTypeList (S : signature) (lt : list methType) (be : beta_exp) : beta_exp :=
  match lt with
  | [] => be
  | t :: rlt => (beta_e_contAssign (CA (applyListOfMethContractsFromMethTypeList S rlt be) t))
  end.
 *)
Fixpoint expFromFieldName (f: fieldName) (F: list ((fieldName * ty) * beta_exp)) : option beta_exp :=
  match F with
  | [] => None
  | (((f1, t1), e1) :: F2) => match (beq_fName f f1) with
                                      | true => Some e1
                                      | false => (expFromFieldName f F2)
                                      end
  end.
  
Fixpoint valFromFieldName (f: fieldName) (F : list ((fieldName * ty) * beta_val)) : option beta_val :=
  match F with
  | [] => None
  | (((f1, t1), v1) :: F2) => match (beq_fName f f1) with
                                      | true => Some v1
                                      | false => (valFromFieldName f F2)
                                      end
  end.
    
Fixpoint fieldInList (f: fieldName) (fns: list (fieldName * ty)) : bool :=
  match fns with
  | [] => false
  | (f1, t1) :: fns1 => match (beq_fName f f1) with
                  | true => true
                  | false => (fieldInList f fns1)
                  end
  end.

Definition objContainsField (O: beta_objRep) (f: fieldName) : bool :=
  match O with
  | nulObj => false
 (* | (bobj_fDecl l ms fdc cs) => false (* looking for field DEFN here; not enough to just have decl, b/c need value *)*)
  | (bobj_fDefn l ms (beta_fDefn fns fdcs) cs) => (fieldInList f fns)
  end. 

Definition getFieldsFromObj (O: beta_objRep) : list ((fieldName * ty) * beta_exp) := 
  match O with
  | nulObj => []
 (* | (bobj_fDecl l ms fdc cs) => [] (* looking for field DEFN here; not enough to just have decl, b/c need value *)*)
  | (bobj_fDefn l ms (beta_fDefn fns fdcs) cs) => (combine fns fdcs)
  end.
  
Definition getFieldsFromRunObj (O: beta_runtimeObj) : list ((fieldName * ty) * beta_val) := 
  match O with
  | nulrobj => []
 (* | (bobj_fDecl l ms fdc cs) => [] (* looking for field DEFN here; not enough to just have decl, b/c need value *)*)
  | (brobj_fDefn l ms (beta_vfDefn fns fdcs) cs) => (combine fns fdcs)
  end.
  
(* should this even be a thing?  or should i just return directly from location which is string anyway*)
(* this only works if loc is an id i.e. a string *)
(* Definition stringFromLoc (l: location) : val :=
  match l with
  | nulLoc => (v_lit_prim (StrLit EmptyString) prim_String)
  | thisLoc (Id b) => (v_lit_prim (StrLit b) prim_String)
  end.*)


(* this is redundant now bc of findLoc which has now been renamed locFromVarInStore *)
(*  
Fixpoint locFromVarInStore (L: beta_localStore) (x: var) : location := 
  match L with
  | [] => nulLoc
  | (v, l) :: L1 => match (beq_var v x) with
                    | true => l
                    | false => (locFromVarInStore L1 x)
                    end
  end.*)


(* | bobj_fDecl   : location -> list methDecl -> option fieldDecl -> list beta_contracts -> beta_objRep
   | bobj_fDefn   : location -> list beta_methDefn -> beta_fieldDefn -> list beta_contracts -> beta_objRep. *)
Definition addContractToObj (S: signature) (t : ty) (obj1 : beta_runtimeObj) : beta_runtimeObj :=
  match obj1 with
  | nulrobj => nulrobj
 (* | bobj_fDecl l lmds ofd lk => bobj_fDecl l lmds ofd ((toContract S t) :: lk) *)
  | brobj_fDefn l lmds fd lk => brobj_fDefn l lmds fd ((toContract S t) :: lk)
  end.
  
(*Fixpoint updateObjWithContract (H : beta_heap) (l : location) (t : ty) (S : signature) : beta_heap :=
  match H with
  | [] => []
  | (l1, (bheap_cont_prim lp pt)) :: rH => updateObjWithContract rH l t S
  | (l1, (bheap_cont_obj nulrobj)) :: rH => updateObjWithContract rH l t S
  | (l1, (bheap_cont_obj (brobj_fDefn l2 M1 F1 K))) :: rH => match (beq_loc l1 l) with
                                        | false =>  updateObjWithContract rH l t S
                                        | true => (l, (bheap_cont_obj (addContractToObj S t (brobj_fDefn l1 M1 F1 K)))) :: rH
                                        end 
  end.*)
Fixpoint updateObjWithContract (H : beta_heap) (l : location) (t : ty) (S1 : signature) (n : nat) : beta_heap :=
  match H with
  | [] => []
  | bhc :: rH => match l with
                 | nulLoc => bhc :: rH
                 | thisLoc n1 => match (beq_nat n1 n) with
                                 | false => bhc :: (updateObjWithContract rH l t S1 (S n))
                                 | true => match bhc with
                                           | (bheap_cont_obj (brobj_fDefn l M1 F1 K)) => (bheap_cont_obj (addContractToObj S1 t (brobj_fDefn l M1 F1 K))) :: rH
                                           | _ => bhc :: rH 
                                           end 
                                 end 
                 end
  end.
  
Definition isObjType (t : ty) : Prop :=
  match t with 
  | prim_dTyp p => False
  | _ => True
  end.
Hint Unfold isObjType.
  
(*Fixpoint fDefnListFromFNTListAndValues (lfnts : list (fieldName * ty)) (es : list beta_exp) : list beta_fieldDefn :=
  match lfnts, es with
  | [], _ => []
  | _, [] => []
  | (fnt :: rfnts), (e :: res) => (beta_fDefn [fnt] [e]) :: (fDefnListFromFNTListAndValues rfnts res)
  end. *)
  
Fixpoint contractAssignOverList_lfnts_exps (lfnts : list (fieldName * ty)) (es : list beta_exp) : list beta_exp :=
  match lfnts, es with
  | [], [] => []
  | [], _ => [beta_e_value (beta_v_ERROR)]
  | _, [] => [beta_e_value (beta_v_ERROR)]
  | ((fn, ft) :: rlfnts), (e :: res) => (beta_e_contAssign (CA e ft)) :: (contractAssignOverList_lfnts_exps rlfnts res)
  end.
  
(* (fDefnListFromDeclsAndValues (getFields C) vs) *)
Definition fDefnListFromDeclsAndValues_andContAssign (fd : option fieldDecl) (es : list beta_exp) : beta_fieldDefn :=
  match fd with
  | None => beta_fDefn [] []
  | Some (fDecl lfnt) => beta_fDefn lfnt (contractAssignOverList_lfnts_exps lfnt es)
  end.
  
Fixpoint errorInListOfExps (es : list beta_exp) : Prop :=
  match es with
  | [] => False
  | e :: res => match e with
                | (beta_e_value (beta_v_ERROR)) => True
                | _ => (errorInListOfExps res)
                end 
  end. 
  (*
  (beta_e_value (beta_v_ERROR)) :: res => True
  | e :: res => (errorInListOfExps res)
  end. *)
  
Fixpoint valsFromExps (le : list beta_exp) : list beta_val :=
  match le with
  | [] => []
  | (beta_e_value v) :: res => v :: (valsFromExps res)
  | _ :: res => (beta_v_ERROR) :: (valsFromExps res)
  end.
  
Definition correspondingValFdefs (fd : beta_fieldDefn) : beta_valFieldDefn :=
  match fd with
  | beta_fDefn lfnts lfes => beta_vfDefn lfnts (valsFromExps lfes)
  end.
  
Fixpoint expsFromVals (le : list beta_val) : list beta_exp :=
  match le with
  | [] => []
  | v :: rvs => (beta_e_value v) :: (expsFromVals rvs)
  end.
    
Definition correspondingExpFdefs (fd : beta_valFieldDefn) : beta_fieldDefn :=
  match fd with
  | beta_vfDefn lfnts lfes => beta_fDefn lfnts (expsFromVals lfes)
  end.
  
(*Definition fieldContractIsSatisfied (*(l: location) (mnds: beta_methDefn) *)(fnds: beta_fieldDefn) (cs: beta_contracts) (fn: fieldName) : Prop :=
  *)
  
Definition beta_field_is_value (fd: beta_fieldDefn) : Prop :=
  match fd with
  | beta_fDefn lfnts lfes => expListAllValues lfes
  end.
Hint Unfold beta_field_is_value.

Definition primTypeMatches (p: lit) (t: primType) : Prop :=
  match p with
  | BoolLit b => match t with 
                 | prim_Bool => True
                 | _ => False
                 end
  | numLit n => match t with 
                | prim_Number => True
                | _ => False
                end
  | StrLit s => match t with
                | prim_String => True
                | _ => True
                end
  | _ => match t with
         | prim_Void => True
         | _ => False
         end
  end.
  
Hint Unfold primTypeMatches.
  
Fixpoint getMethDefContsFromMethDefn (lmnts : list (methName * methType)) (lmcs : list beta_methDef_conts) (m : methName) : option beta_methDef_conts :=
  match lmnts, lmcs with
  | _, [] => None
  | [], _ => None
  | ((mn, mt) :: rlmnt), (mc :: rlmc) => match (beq_mName mn m) with
                                         | true => Some mc
                                         | false => getMethDefContsFromMethDefn rlmnt rlmc m
                                         end
  end. 
  
Fixpoint getMethDefnContsFromMethDefnList (mdfs : list beta_methDefn) (m : methName) : option beta_methDef_conts :=
  match mdfs with
  | [] => None
  | (beta_mDefn lmnts lmcs) :: rmdfs => match (getMethDefContsFromMethDefn lmnts lmcs m) with
                                        | Some md => Some md
                                        | None => (getMethDefnContsFromMethDefnList rmdfs m)
                                        end
  end.

Definition getMethDefnFromRunObj (bo : beta_runtimeObj) (m : methName) : option beta_methDef_conts :=
  match bo with
  | nulrobj => None
  | brobj_fDefn l lmd fd lc => getMethDefnContsFromMethDefnList lmd m
  end.
  
Fixpoint getFieldExpFromFieldList (lfts : list (fieldName * ty)) (lfvs : list beta_val) (f : fieldName) : option beta_exp :=
  match lfts, lfvs with
  | [], _ => None
  | _, [] => None
  | ((fn, tn) :: rfts), (v :: rfvs) => match (beq_fName fn f) with
                                       | true => Some (beta_e_value v)
                                       | _ => (getFieldExpFromFieldList rfts rfvs f)
                                       end 
  end. 
  
Definition getFieldExpFromRunObj (bo : beta_runtimeObj) (f : fieldName) : option beta_exp :=
  match bo with
  | nulrobj => None
  | brobj_fDefn l lmd (beta_vfDefn lfts lfvs) lc => (getFieldExpFromFieldList lfts lfvs f)
  end.
  
Fixpoint getFieldTypesFromOblList (lo : list beta_obls) (f : fieldName) : list ty :=
  match lo with 
  | [] => []
  | (field_obl fn ft) :: ro => match (beq_fName fn f) with
                               | true => ft :: (getFieldTypesFromOblList ro f)
                               | false => (getFieldTypesFromOblList ro f)
                               end 
  | o :: ro => (getFieldTypesFromOblList ro f)
  end.
  
Fixpoint getFieldTypesFromContractList (lc : list beta_contracts) (f : fieldName) : list ty := 
  match lc with
  | [] => []
  | (empty_cont) :: rc => (getFieldTypesFromContractList rc f)
  | (lobls_cont lo) :: rc => (getFieldTypesFromOblList lo f) ++ (getFieldTypesFromContractList rc f) 
  end. 
  
Definition getFieldTypesFromRunObj (bo : beta_runtimeObj) (f : fieldName) : list ty :=
  match bo with
  | nulrobj => []
  | brobj_fDefn l lmd fd lc => (getFieldTypesFromContractList lc f)
  end.

Fixpoint getMethNameTypesFromOblList (lo : list beta_obls) (m : methName) : list (methName * methType) :=
  match lo with 
  | [] => []
  | (memberFct_obl mn mt) :: ro => match (beq_mName mn m) with
                               | true => (m, mt) :: (getMethNameTypesFromOblList ro m)
                               | false => (getMethNameTypesFromOblList ro m)
                               end 
  | o :: ro => (getMethNameTypesFromOblList ro m)
  end.
 
Fixpoint getMethNameTypesFromContractList (lc : list beta_contracts) (m : methName) : list (methName * methType) := 
  match lc with
  | [] => []
  | (empty_cont) :: rc => (getMethNameTypesFromContractList rc m)
  | (lobls_cont lo) :: rc => (getMethNameTypesFromOblList lo m) ++ (getMethNameTypesFromContractList rc m) 
  end. 
  
Definition getMethNameTypesFromRunObj (bo : beta_runtimeObj) (m : methName) : list (methName * methType) :=
  match bo with
  | nulrobj => []
  | brobj_fDefn l lmd fd lc => (getMethNameTypesFromContractList lc m)
  end.
  
Fixpoint updateVarLoc (x : var) (newL : location) (bs : beta_localStore) : beta_localStore :=
  match bs with
  | [] => []
  | (x1, l1) :: rbs => match (beq_var x1 x) with
                       | true => (x1, newL) :: rbs
                       | false => (x1, l1) :: (updateVarLoc x newL rbs)
                       end
  end.

(* Reduction rules *)
Reserved Notation "e1 '/' st1 '-->' e2 '/' st2"
  (at level 40, st1 at level 39, e2 at level 39). 
Reserved Notation "e1 '/' st1 '~~>' e2 '/' st2"
  (at level 40, st1 at level 39, e2 at level 39). 
  
(* compiling from post-type-comp stat to post-type-comp stat (this is the runtime grammar) *)
(* TODO what do i do with the error *)
Inductive reduction : beta_state * beta_stat -> beta_state * beta_stat -> Prop :=
 (* configuration reduction for statements *)
 | beta_E_Val    : forall C v, 
                    (*~ (v = (beta_s_exp (beta_e_value (beta_v_lit_prim DieLit prim_Void)))) ->*) (* I assume die is type void *)
                    ~ (v = (beta_s_exp (beta_e_value beta_v_ERROR))) -> 
                    (*~ (v = (beta_s_exp (beta_e_value (beta_v_loc l)))) -> *)
                    value v ->
                    C / v --> C / beta_s_skip 
 (*| beta_E_Val_badLoc : forall S H X L loc1,
                       (findHeapContsAtLoc H loc1) = (bheap_cont_obj nulrobj) ->
                       (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_Val_primLoc : forall S H X L loc1 p1 pt1,
                        (findHeapContsAtLoc H loc1) = (bheap_cont_prim p1 pt1) ->   
                        (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_lit_prim p1 pt1)))
 | beta_E_Val_runObj : forall S H X L loc1 l mdef fdef cs,
                       (findHeapContsAtLoc H loc1) = (bheap_cont_obj (brobj_fDefn l mdef fdef cs)) ->  
                       (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) --> (bstate_state S H X L) / (beta_s_exp (beta_e_obj (bobj_fDefn l mdef (correspondingExpFdefs fdef) cs))) 
 *)
 
 | beta_E_Seq_Skip: forall C s,
                   C / (beta_s_stats beta_s_skip s) --> C / s 
 | beta_E_Seq_canRed: forall C s1 s2 s1' C',
                      C / s1 --> C' / s1' ->
                      s1' <> (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                      C / (beta_s_stats s1 s2) --> C' / (beta_s_stats s1' s2) 
 | beta_E_Seq_canRed_ERR: forall C s1 s2 s1' C',
                      C / s1 --> C' / s1' ->
                      s1' = (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                      C / (beta_s_stats s1 s2) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) 
 | beta_E_Seq_ERR: forall C s,
                   C / (beta_s_stats (beta_s_exp (beta_e_value beta_v_ERROR)) s) --> C / (beta_s_exp (beta_e_value beta_v_ERROR))
 (* variable definition *)
 | beta_E_VarDef_canRed : forall C x tau C' e e',
                       C / e ~~> C' / e' ->
                       e' <> (beta_e_value beta_v_ERROR) ->
                       C / (beta_s_varAssign x tau e) --> C' / (beta_s_varAssign x tau e')
 | beta_E_VarDef_loc : forall S H X L L' x tau e n,
                       e = (beta_e_value (beta_v_loc (thisLoc n))) ->
                       L' = (x, (thisLoc n)) :: L ->
                       (bstate_state S H X L) / (beta_s_varAssign x tau e) --> (bstate_state S H X L') / beta_s_skip
 | beta_E_VarDef_loc_nil : forall C e x tau,
                           e = (beta_e_value (beta_v_loc nulLoc)) ->
                           C / (beta_s_varAssign x tau e) --> C / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_VarDef_canRed_ERR : forall C C' x tau e,
                       C / (beta_s_exp e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                       C / (beta_s_varAssign x tau e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))
 (*| beta_E_VarDef_obj_ok : forall S H X L x obj tau H',
                          (isObjType tau) ->   
                          H' = ((locFromVarInStore L x), (bheap_cont_obj (addContractToObj S tau obj))) :: H -> 
                          (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value (beta_v_runObj obj))) -->
                                         (bstate_state S H' X L) / (beta_s_skip)
 | beta_E_VarDef_obj_badType : forall S H X L x obj tau, (* equivalent to the contractAssign where the type is not an object type *)
                               ~(isObjType tau) ->   
                               (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value (beta_v_runObj obj))) -->
                                                        (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))*)
 | beta_E_VarDef_prim_ok : forall S H X L L' x p t tau H', 
                           (primTypeMatches p t) ->
                           H' = ((*(locFromVarInStore L x), *)(bheap_cont_prim p t)) :: H -> 
                           L' = (x, (thisLoc (stLength H'))) :: L ->
                           (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value (beta_v_lit_prim p t))) -->
                                         (bstate_state S H' X L') / (beta_s_skip)
 | beta_E_VarDef_prim_badType : forall S H X L x p t tau, 
                                ~(primTypeMatches p t) ->
                                (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value (beta_v_lit_prim p t))) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_VarDef_prim_objType : forall S H X L x p t tau, 
                                (isObjType tau) ->
                                (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value (beta_v_lit_prim p t))) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_VarDef_ERR : forall S H X L x tau,
                       (bstate_state S H X L) / (beta_s_varAssign x tau (beta_e_value beta_v_ERROR)) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 (* variable update *)
 | beta_E_VarUpd_canRed : forall C x C' e e',
                    C / e ~~> C' / e' ->
                    e' <> (beta_e_value beta_v_ERROR) ->
                    C / (beta_s_assignNoVar x e) --> C' / (beta_s_assignNoVar x e')
 | beta_E_VarUpd_canRed_ERR : forall C C' x e,
                       C / (beta_s_exp e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                       C / (beta_s_assignNoVar x e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))
 (*| beta_E_VarUpd_obj : forall S H X L x obj H',
                    ~ ((beq_loc (locFromVarInStore L x) nulLoc) = true) -> (* i.e. var already exists (updating, not defining) *)
                    H' = ((locFromVarInStore L x), (bheap_cont_obj obj)) :: H -> 
                    (bstate_state S H X L) / (beta_s_assignNoVar x (beta_e_value (beta_v_runObj obj))) -->
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_runObj obj)))*)
 | beta_E_VarUpd_prim : forall S H X L x p t H' L',
                    ~ ((beq_loc (locFromVarInStore L x) nulLoc) = true) -> (* i.e. var already exists (updating, not defining) *)
                    H' = ((*(locFromVarInStore L x), *)(bheap_cont_prim p t)) :: H -> 
                    L' = (updateVarLoc x (thisLoc (stLength H')) L) ->
                    (bstate_state S H X L) / (beta_s_assignNoVar x (beta_e_value (beta_v_lit_prim p t))) -->
                                         (bstate_state S H' X L') / (beta_s_exp (beta_e_value (beta_v_lit_prim p t)))
 | beta_E_VarUpd_loc : forall S H X L L' x n,
                       ~ ((beq_loc (locFromVarInStore L x) nulLoc) = true) -> (* i.e. var already exists (updating, not defining) *)
                       L' = (updateVarLoc x (thisLoc n) L) ->
                       (bstate_state S H X L) / (beta_s_assignNoVar x (beta_e_value (beta_v_loc (thisLoc n)))) --> (bstate_state S H X L') / (beta_s_exp (beta_e_value (beta_v_loc (thisLoc n))))
 | beta_E_VarUpd_loc_nil : forall C x,
                           C / (beta_s_assignNoVar x (beta_e_value (beta_v_loc nulLoc))) --> C / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_VarUpd_ERR : forall S H X L x,
                   (bstate_state S H X L) / (beta_s_assignNoVar x (beta_e_value beta_v_ERROR)) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_VarUpd_BadLoc : forall S H X L x v,
                          ((beq_loc (locFromVarInStore L x) nulLoc) = true) -> (* i.e. var doesnt exist *)
                          (bstate_state S H X L) / (beta_s_assignNoVar x (beta_e_value v)) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 (* static field update *)
 | beta_E_SfldUpd_firstRed : forall C C' e1 e2 e1' f,
                             C / e1 ~~> C' / e1' ->
                             e1' <> (beta_e_value beta_v_ERROR) ->
                             C / (beta_s_fieldAssign e1 f e2) --> C' / (beta_s_fieldAssign e1' f e2)
 | beta_E_SfldUpd_firstRed_ERR : forall C e1 e2 f C',
                       C / (beta_s_exp e1) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                       C / (beta_s_fieldAssign e1 f e2) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_SfldUpd_firstBadVal : forall C ev e f l,
                                (value (beta_s_exp ev)) ->
                                ~ (ev = (beta_e_value (beta_v_loc l))) ->
                                C / (beta_s_fieldAssign ev f e) --> C / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_SfldUpd_canRed : forall C C' l f e e',
                    C / e ~~> C' / e' ->
                    e' <> (beta_e_value beta_v_ERROR) -> 
                    C / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f e) -->
                                         C' / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f e')
 | beta_E_SfldUpd_canRed_ERR : forall C C' l f e,
                       C / (beta_s_exp e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                       C / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))        
 (*| beta_E_SfldUpd_obj_ok : forall S H X L l f fd md cs H' l1,
                    H' = (updateObjFieldWithVal H f l (beta_v_runObj (brobj_fDefn l1 fd md cs))) ->
                    (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f (beta_e_value (beta_v_runObj (brobj_fDefn l1 fd md cs)))) -->
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_runObj (brobj_fDefn l1 fd md cs))))
 | beta_E_SfldUpd_obj_bad : forall S H X L l f H',
                    H' = (updateObjFieldWithVal H f l (beta_v_runObj nulrobj)) ->
                    (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f (beta_e_value (beta_v_runObj nulrobj))) -->
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value beta_v_ERROR))*)
 | beta_E_SfldUpd_prim : forall S H X L l f p t H',
                    H' = (updateObjFieldWithVal H f l (beta_v_lit_prim p t) 0) ->
                    (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f (beta_e_value (beta_v_lit_prim p t))) -->
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_lit_prim p t)))
 (* dont need to worry about nulrobj as this will be caught on the next step *)
 (*| beta_E_SfldUpd_loc_obj : forall S H X L l1 l2 f obj,
                           ((findHeapContsAtLoc H l2) = (bheap_cont_obj obj)) ->
                           (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l1)) f (beta_e_value (beta_v_loc l2))) -->
                                     (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l1)) f (beta_e_value (beta_v_runObj obj)))
 | beta_E_SfldUpd_loc_prim : forall S H X L l1 l2 f p pt,
                           ((findHeapContsAtLoc H l2) = (bheap_cont_prim p pt)) ->
                           (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l1)) f (beta_e_value (beta_v_loc l2))) -->
                                     (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l1)) f (beta_e_value (beta_v_lit_prim p pt)))*)
 | beta_E_SfldUpd_loc : forall S H X L l1 l2 f H',
                        H' = (updateObjFieldWithVal H f l1 (beta_v_loc l2) 0) ->
                        (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l1)) f (beta_e_value (beta_v_loc l2))) -->
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_loc l2)))
 | beta_E_SfldUpd_ERR : forall S H X L l f,
                    (bstate_state S H X L) / (beta_s_fieldAssign (beta_e_value (beta_v_loc l)) f (beta_e_value beta_v_ERROR)) -->
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 (* dynamic field update *)
 | beta_E_DFldUpdLit_canRed : forall S H X L l cv e e' H',
                     (bstate_state S H X L) / (beta_s_exp e) --> (bstate_state S H' X L) / (beta_s_exp e') ->
                     (bstate_state S H X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String)) e) --> 
                                         (bstate_state S H' X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String)) e')
 (*| beta_E_DFldUpdLit_obj : forall S H X L l cv obj H' f,
                     (* TODO fix u probably dont need any of this toString bs, it's just overcomplicating matters i think *)
                     f = (toString cv) -> (* there should be a check maybe that this isn't the empty string *)
                     (*~ ((beq_str f EmptyString) = true) ->*)
                     H' = (updateObjFieldWithVal H (Field (Id f)) l (beta_v_runObj obj)) -> 
                     (bstate_state S H X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String)) (beta_e_value (beta_v_runObj obj))) --> 
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_runObj obj)))*)
 | beta_E_DFldUpdLit_loc : forall S H X L H' l1 l2 f cv,
                           f = (toString cv) ->
                           H' = (updateObjFieldWithVal H (Field (Id f)) l1 (beta_v_loc l2) 0) -> 
                           (bstate_state S H X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l1)) (beta_e_value (beta_v_lit_prim cv prim_String)) (beta_e_value (beta_v_loc l2))) --> 
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_loc l2)))
 | beta_E_DFldUpdLit_prim : forall S H X L l cv p t H' f,
                     (* TODO fix u probably dont need any of this toString bs, it's just overcomplicating matters i think *)
                     f = (toString cv) -> (* there should be a check maybe that this isn't the empty string *)
                     (*~ ((beq_str f EmptyString) = true) ->*)
                     H' = (updateObjFieldWithVal H (Field (Id f)) l (beta_v_lit_prim p t) 0) -> 
                     (bstate_state S H X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String)) (beta_e_value (beta_v_lit_prim p t))) --> 
                                         (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_lit_prim p t)))
 | beta_E_DFldUpdLit_ERR : forall S H X L l cv,
                     (bstate_state S H X L) / (beta_s_accAssign (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String)) (beta_e_value beta_v_ERROR)) --> 
                                         (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))
 (* This one is weird | beta_E_DFldUpdLoc : forall C l l' v,*)
 | beta_E_If_boolVal_true : forall C iv s1 s2 v,
                            iv = (beta_v_lit_prim (BoolLit v) prim_Bool) ->
                            v = true ->
                            C / (beta_s_ifElse (beta_e_value iv) s1 s2) --> C / s1
 | beta_E_If_boolVal_false : forall C iv s1 s2 v,
                             iv = (beta_v_lit_prim (BoolLit v) prim_Bool) ->
                             v = false ->
                             C / (beta_s_ifElse (beta_e_value iv) s1 s2) --> C / s2
 | beta_E_If_badVal : forall C iv s1 s2 v,
                      ~ (iv = (beta_v_lit_prim (BoolLit v) prim_Bool)) ->
                      C / (beta_s_ifElse (beta_e_value iv) s1 s2) --> C / (beta_s_exp (beta_e_value beta_v_ERROR))
 | beta_E_If_canRed : forall C C' e e' s1 s2, 
                      C / e ~~> C' / e' ->
                      e' <> (beta_e_value beta_v_ERROR) ->
                      C / (beta_s_ifElse e s1 s2) --> C' / (beta_s_ifElse e' s1 s2)
 | beta_E_If_canRed_err : forall C e s1 s2 C',
                          C / (beta_s_exp e) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)) ->
                          C / (beta_s_ifElse e s1 s2) --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))
 (*| beta_E_SCxt : forall C E s C' s', (* this is probably wrong O .o  -- also should encompass the defn for die*)
                  (C', s') = (evalWithStat E C s) -> 
                  C / s --> C' / s'*)
 (* do we need SDie *)
 
 | beta_E_e_steps : forall C C' e e',
                    C / e ~~> C' / e' ->
                    (*e' <> (beta_e_value beta_v_ERROR) ->*)
                    C / (beta_s_exp e) --> C' / (beta_s_exp e')
 
(* | beta_E_SMCall_M_S : forall S H X L ev vis m l vts t s e1 obj, 
                       (value (beta_s_exp ev)) ->
                       (ev = (beta_e_value (beta_v_loc l))) ->
                       expListAllValues vis ->
                       ~(errorInListOfExps vis) ->
                       (findHeapContsAtLoc H l) = (bheap_cont_obj obj) ->
                       (Some (beta_mD_conts vts t s e1) = (getMethDefnFromRunObj obj m)) -> 
                       (bstate_state S H X L) / (beta_s_exp (beta_e_methCall ev m vis)) --> (bstate_state S H X L) / (subst_stat (combine (fst (split vts)) vis) s) 
*)
where "e1 '/' st1 '-->' e2 '/' st2" := (reduction (e1,st1) (e2,st2))

with reduction_exps : beta_state * beta_exp -> beta_state * beta_exp -> Prop :=
  
 (* configuration reduction for expressions (using s_exp to convert to stat -- NOT ANYMORE) *)
 (* | beta_E_ECxt : dont need this one since it's the same as beta_E_SCxt just with exp instead of stat *)
 (* | same with beta_E_EDie *) 
 | beta_E_ObjLit_firstTR_DH : forall C Mdef Fdef,
                              C / (beta_e_defFirstTR Mdef Fdef) ~~> C / (beta_e_theDefns Mdef Fdef)
 | beta_E_ObjLit_canReduce : forall C C' Mdef fnts fes fes',
                             ListReduction (C, fes) (C', fes') ->
                             ~ (errorInListOfExps fes') ->
                             C / (beta_e_theDefns Mdef (beta_fDefn fnts fes)) ~~> C' / (beta_e_theDefns Mdef (beta_fDefn fnts fes')) 
 | beta_E_ObjLit_canRed_ERR : forall C C' Mdef fnts fes fes',
                             ListReduction (C, fes) (C', fes') ->
                             (errorInListOfExps fes') ->
                             C / (beta_e_theDefns Mdef (beta_fDefn fnts fes)) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_ObjLit_isVal : forall S H X L Mdef Fdef,
                         (*l = (thisLoc (stLength H)) ->
                         O = (bobj_fDefn l [Mdef] Fdef []) ->
                         objIsValue O ->*)
                         beta_field_is_value Fdef ->
                         (*H' = stSnoc H (l, (bheap_cont_obj O)) ->*)
                         (bstate_state S H X L) / (beta_e_theDefns Mdef Fdef) ~~> (bstate_state S H X L) / (beta_e_obj (bobj_fDefn (thisLoc (stLength H)) [Mdef] Fdef []))
 | beta_E_Obj_redToRunObj : forall Mdef Fdef cs rFdefs S H X L H' l l',
                            objIsValue (bobj_fDefn l Mdef Fdef cs) ->
                            rFdefs = (correspondingValFdefs Fdef) ->
                            l' = (thisLoc (stLength H)) ->
                            H' = stSnoc H ((*l', *)(bheap_cont_obj (brobj_fDefn l' Mdef rFdefs cs))) ->
                            (bstate_state S H X L) / (beta_e_obj (bobj_fDefn l Mdef Fdef cs)) ~~> (bstate_state S H' X L) / (beta_e_value (beta_v_loc l'))
 (*| beta_E_runObj_addToHeap : forall S H X L H' l l' mdef fdef cs,
                             l' = (thisLoc (stLength H)) ->
                             H' = stSnoc H (l, (bheap_cont_obj (brobj_fDefn l' mdef fdef cs))) ->
                             (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_runObj (brobj_fDefn l mdef fdef cs)))) --> (bstate_state S H' X L) / (beta_s_exp (beta_e_value (beta_v_runObj (brobj_fDefn l' mdef fdef cs))))
*)
 | beta_E_Obj_canRed : forall C C' Mdef fnts fes fes' l cs,
                       ListReduction (C, fes) (C', fes') ->
                       ~ (errorInListOfExps fes') ->
                       C / (beta_e_obj (bobj_fDefn l Mdef (beta_fDefn fnts fes) cs)) ~~> C' / (beta_e_obj (bobj_fDefn l Mdef (beta_fDefn fnts fes') cs))
 | beta_E_Obj_canRed_ERR : forall C C' Mdef fnts fes fes' l cs,
                       ListReduction (C, fes) (C', fes') ->
                       (errorInListOfExps fes') ->
                       C / (beta_e_obj (bobj_fDefn l Mdef (beta_fDefn fnts fes) cs)) ~~> C' / (beta_e_value beta_v_ERROR)

 | beta_E_New_canRed : forall C C' Cn vs vs',
                       ListReduction (C, vs) (C', vs') ->
                       C / (beta_e_newClass Cn vs) ~~> C' / (beta_e_newClass Cn vs')
 | beta_E_New_NotAllVals : forall S X H L vs C Cn,
                           C = (getClass S Cn) ->
                           ~ (expListAllValues vs) ->
                           (bstate_state S H X L) / (beta_e_newClass Cn vs) ~~> (bstate_state S H X L) / (beta_e_value (beta_v_ERROR))
 | beta_E_New : forall S H (*H'*) X L (*l Mdef O*) vs C Cn  (*fvs*) (*Cd1*),
                (*l = (thisLoc (stLength H)) ->*)
                C = (getClass S Cn) ->
                (*(C = Some Cd1) ->*)
                (*fs = (getFields C) -> *)
                expListAllValues vs ->
                (*fvs = (fDefnListFromDeclsAndValues_andContAssign (getFields C) vs) -> *)
                ~(errorInListOfExps vs) ->
                (*Mdef  = (getMethods C) ->
                O = (bobj_fDefn (thisLoc (stLength H))  (*getMethDeclListFromDefnList *)(getMethodList (getMethods C)) (fDefnListFromDeclsAndValues_andContAssign (getFields C) vs) [(toContract S (class_dTyp Cn))]) -> *)
                (*H' = stSnoc H (l, (bheap_cont_obj O)) ->*)
                (bstate_state S H X L) / (beta_e_newClass Cn vs) ~~> (bstate_state S H X L) / (beta_e_obj (bobj_fDefn (thisLoc (stLength H)) (getMethodList (getMethods C)) (fDefnListFromDeclsAndValues_andContAssign (getFields C) vs) [(toContract S (class_dTyp Cn))])) 
 | beta_E_New_wrongParamErr : forall S X H L (*l Mdef O*) vs C Cn  (*fvs*) (*Cd1*),
                              (*l = (thisLoc (stLength H)) ->*)
                              C = (getClass S Cn) ->
                              (*(C = Some Cd1) ->*)
                              (*fs = (getFields C) -> *)
                              expListAllValues vs ->
                              (*fvs = (fDefnListFromDeclsAndValues_andContAssign (getFields C) vs) -> *)
                              (errorInListOfExps vs) ->
                              (bstate_state S H X L) / (beta_e_newClass Cn vs) ~~> (bstate_state S H X L) / (beta_e_value (beta_v_ERROR))
 (*| beta_E_New_noClass : forall S H X L Cn vs,
                        None = (getClass S Cn) ->
                        (bstate_state S H X L) / (beta_s_exp (beta_e_newClass Cn vs)) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_ERROR)))*)
 (* splitting up E_Sproj case into 2 cases, one where the field exists *)
 | beta_E_Sproj_vThere : forall S H X L l v' f fds O v,
                         O = (findObjInHeap H l) ->
                         fds = (getFieldsFromRunObj O) ->
                         v' = (valFromFieldName f fds) ->
                         v' = (Some v) ->
                         (bstate_state S H X L) / (beta_e_fieldAcc (beta_e_value (beta_v_loc l)) f) ~~> (bstate_state S H X L) / (beta_e_value v)
 (* and one case where the field is not there *)
 | beta_E_Sproj_vNotThere : forall S H X L l v' f fds O,
                            O = (findObjInHeap H l) ->
                            fds = (getFieldsFromRunObj O) ->
                            v' = (valFromFieldName f fds) ->
                            v' = None ->
                            (bstate_state S H X L) / (beta_e_fieldAcc (beta_e_value (beta_v_loc l)) f) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)  
 | beta_E_Dprojlit_vThere : forall S H X L l v' cv f fds O v,
                            f = (Field (Id (toString cv))) ->
                            O = (findObjInHeap H l) ->
                            fds = (getFieldsFromRunObj O) ->
                            v' = (valFromFieldName f fds) ->
                            v' = (Some v) ->
                            (bstate_state S H X L) / (beta_e_access (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String))) ~~> (bstate_state S H X L) / (beta_e_value v)
 (* and one case where the field is not there *)
 | beta_E_Dprojlit_vNotThere : forall S H X L l v' f cv fds O,
                               f = (Field (Id (toString cv))) ->
                               O = (findObjInHeap H l) ->
                               fds = (getFieldsFromRunObj O) ->
                               v' = (valFromFieldName f fds) ->
                               v' = None ->
                               (bstate_state S H X L) / (beta_e_access (beta_e_value (beta_v_loc l)) (beta_e_value (beta_v_lit_prim cv prim_String))) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)  
 (*| beta_E_Dprojloc : forall C l l' fl,
                     fl = (stringFromLoc l') ->
                     C / (s_exp (e_access (e_value (v_loc l)) (e_value (v_loc l)))) --> C / (s_exp (e_access (e_value (v_loc l)) (e_value (v_lit_prim fl prim_String))))
*) (* this one is awkward, b/c i think location should really be nat not id *)
 
 | beta_E_Var_obj : forall S H X L x l obj,
                (thisLoc l) = (locFromVarInStore L x) ->
                (bheap_cont_obj obj) = (findHeapContsAtLoc H (thisLoc l)) ->
                (bstate_state S H X L) / (beta_e_var x) ~~> (bstate_state S H X L) / (beta_e_value (beta_v_loc (thisLoc l)))
 | beta_E_Var_prim : forall S H X L x l p t,
                (thisLoc l) = (locFromVarInStore L x) ->
                (bheap_cont_prim p t) = (findHeapContsAtLoc H (thisLoc l)) ->
                (bstate_state S H X L) / (beta_e_var x) ~~> (bstate_state S H X L) / (beta_e_value (beta_v_lit_prim p t))
 | beta_E_Var_noLoc : forall S H X L x,
                nulLoc = (locFromVarInStore L x) ->
                (*~ (l = nulLoc) ->*)
                (bstate_state S H X L) / (beta_e_var x) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
                
 (* reduction rules for method calls *)
 | beta_E_SMCall_OutRed : forall C C' e e' eis m,
                          C / e ~~> C' / e' ->
                          e' <> (beta_e_value beta_v_ERROR) -> 
                          C / (beta_e_methCall e m eis) ~~> C' / (beta_e_methCall e' m eis)
 | beta_E_SMCall_OutRed_ERR : forall C e eis m C',
                              C / e ~~> C' / (beta_e_value beta_v_ERROR) ->
                              C / (beta_e_methCall e m eis) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_SMCall_OutBad : forall C ev eis m l,
                          (value (beta_s_exp ev)) ->
                          ~(ev = (beta_e_value (beta_v_loc l))) ->
                          C / (beta_e_methCall ev m eis) ~~> C / (beta_e_value beta_v_ERROR)
 | beta_E_SMCall_InRed : forall C C' ev eis eis' m l,
                         (value (beta_s_exp ev)) ->
                         (ev = (beta_e_value (beta_v_loc l))) ->
                         ListReduction (C, eis) (C', eis') ->
                         C / (beta_e_methCall ev m eis) ~~> C' / (beta_e_methCall ev m eis')
 
 | beta_E_SMCall_InErrList : forall C ev vis m l,
                             (value (beta_s_exp ev)) ->
                             (ev = (beta_e_value (beta_v_loc l))) ->
                             expListAllValues vis ->
                             errorInListOfExps vis ->
                             C / (beta_e_methCall ev m vis) ~~> C / (beta_e_value beta_v_ERROR)
 | beta_E_SMCall_locPrim : forall S H X L ev vis m l lp pt,
                           (value (beta_s_exp ev)) ->
                           (ev = (beta_e_value (beta_v_loc l))) ->
                           expListAllValues vis ->
                           ~(errorInListOfExps vis) -> 
                           (findHeapContsAtLoc H l) = (bheap_cont_prim lp pt) ->
                           (bstate_state S H X L) / (beta_e_methCall ev m vis) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_SMCall_NoM : forall S H X L ev vis m l obj,
                       (value (beta_s_exp ev)) ->
                       (ev = (beta_e_value (beta_v_loc l))) ->
                       expListAllValues vis ->
                       ~(errorInListOfExps vis) ->
                       (findHeapContsAtLoc H l) = (bheap_cont_obj obj) ->
                       (None = (getMethDefnFromRunObj obj m)) -> 
                       (bstate_state S H X L) / (beta_e_methCall ev m vis) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_SMCall_M_S : forall S H X L ev vis m l vts t s e1 obj, 
                       (value (beta_s_exp ev)) ->
                       (ev = (beta_e_value (beta_v_loc l))) ->
                       expListAllValues vis ->
                       ~(errorInListOfExps vis) ->
                       (findHeapContsAtLoc H l) = (bheap_cont_obj obj) ->
                       (Some (beta_mD_conts vts t s e1) = (getMethDefnFromRunObj obj m)) -> 
                       (bstate_state S H X L) / (beta_e_methCall ev m vis) ~~> (bstate_state S H X L) / e1 

 | beta_E_FTCast_DH : forall C e t,
                      C / (beta_e_FTcast t e) ~~> C / (beta_e_cast t e)
 | beta_E_cast_canRed : forall C C' e e' t,
                        C / e ~~> C' / e' ->
                        e' <> (beta_e_value beta_v_ERROR) -> 
                        C / (beta_e_cast t e) ~~> C' / (beta_e_cast t e') 
 | beta_E_cast_canRed_ERR : forall C C' e t,
                            C / e ~~> C' / (beta_e_value beta_v_ERROR) ->
                            C / (beta_e_cast t e) ~~> C' / (beta_e_value beta_v_ERROR) 
 | beta_E_cast_isVal : forall C ev t,
                       (value (beta_s_exp ev)) ->
                       C / (beta_e_cast t ev) ~~> C / ev
 (* reduction rules for the contractAssign and checkContract aux functions *)
 (* need some for when these reduce to values *)
 | beta_E_contractAssign_canReduce : forall C C' e1 e1' tau,
                                     C / (beta_s_exp e1) --> C' / (beta_s_exp e1') ->
                                     C / (beta_e_contAssign (CA e1 tau)) ~~> C' / (beta_e_contAssign (CA e1' tau))

 | beta_E_contractAssign_onPrimVal : forall C p1 t pt1,
                                    (* C / (beta_s_exp e1) --> C / (beta_s_exp (beta_e_value (beta_v_lit_prim p1 pt1))) ->  *)
                                     t = (prim_dTyp pt1) ->
                                     C / (beta_e_contAssign (CA (beta_e_value (beta_v_lit_prim p1 pt1)) t)) ~~> C / (beta_e_value (beta_v_lit_prim p1 pt1)) 
 | beta_E_contractAssign_onPrimVal_badType : forall C p1 t pt1,
                                    (* C / (beta_s_exp e1) --> C / (beta_s_exp (beta_e_value (beta_v_lit_prim p1 pt1))) ->  *)
                                    ~(t = (prim_dTyp pt1)) ->
                                     C / (beta_e_contAssign (CA (beta_e_value (beta_v_lit_prim p1 pt1)) t)) ~~> C / (beta_e_value beta_v_ERROR)
                                    
(* | beta_E_contractAssign_onObj_badType : forall S H X L obj1 t1,
                                 (*(bstate_state S H X L) / (beta_s_exp e1) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_runObj obj1))) -> *)
                                 ~(isObjType t1) ->
                                 (bstate_state S H X L) / (beta_s_exp (beta_e_contAssign (CA (beta_e_value (beta_v_runObj obj1)) t1))) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value beta_v_ERROR))                                   
*) | beta_E_contractAssign_onErr : forall C t,
                                 C / (beta_e_contAssign (CA (beta_e_value beta_v_ERROR) t)) ~~> C / (beta_e_value beta_v_ERROR)
 
 (*| beta_E_contractAssign_onObj : forall S H X L obj1 t1,
                                 (*(bstate_state S H X L) / (beta_s_exp e1) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_runObj obj1))) -> *)
                                 (isObjType t1) ->
                                 (bstate_state S H X L) / (beta_s_exp (beta_e_contAssign (CA (beta_e_value (beta_v_runObj obj1)) t1))) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_runObj (addContractToObj S t1 obj1))))                                   
   *)                                     
 | beta_E_contractAssign_onLoc_primVal : forall S H X L loc1 t1 p1 pt1,
                                         (*(bstate_state S H X L) / (beta_s_exp e1) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) ->*)
                                         (findHeapContsAtLoc H loc1) = (bheap_cont_prim p1 pt1) ->                              
                                         (bstate_state S H X L) / (beta_e_contAssign (CA (beta_e_value (beta_v_loc loc1)) t1)) ~~> (bstate_state S H X L) / (beta_e_contAssign (CA (beta_e_value (beta_v_lit_prim p1 pt1)) t1))
 
 | beta_E_contractAssign_onLoc_runObj_badType : forall S H X L loc1 t1 mdef fdef l cs,
                                         (*(bstate_state S H X L) / (beta_s_exp e1) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) ->*)
                                         (findHeapContsAtLoc H loc1) = (bheap_cont_obj (brobj_fDefn l mdef fdef cs)) ->  
                                          ~(isObjType t1) ->                             
                                         (bstate_state S H X L) / (beta_e_contAssign (CA (beta_e_value (beta_v_loc loc1)) t1)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_contractAssign_onLoc_runObj : forall S H X L loc1 t1 mdef fdef l cs H',
                                         (*(bstate_state S H X L) / (beta_s_exp e1) --> (bstate_state S H X L) / (beta_s_exp (beta_e_value (beta_v_loc loc1))) ->*)
                                         (findHeapContsAtLoc H loc1) = (bheap_cont_obj (brobj_fDefn l mdef fdef cs)) ->  
                                         H' = (updateObjWithContract H loc1 t1 S 0) ->                           
                                         (bstate_state S H X L) / (beta_e_contAssign (CA (beta_e_value (beta_v_loc loc1)) t1)) ~~> (bstate_state S H' X L) / (beta_e_value (beta_v_loc loc1))(*(beta_e_contAssign (CA (beta_e_obj (bobj_fDefn l mdef (correspondingExpFdefs fdef) cs)) t1)))*)

 
 | beta_E_contractAssign_onLoc_badLoc : forall S H X L loc1 t1,
                                        (findHeapContsAtLoc H loc1) = (bheap_cont_obj nulrobj) ->
                                        (bstate_state S H X L) / (beta_e_contAssign (CA (beta_e_value (beta_v_loc loc1)) t1)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)                                           
 | beta_E_checkContract_canReduce_meth : forall C e1 e1' s C',
                                         C / e1 ~~> C' / e1' ->
                                         e1' <> (beta_e_value beta_v_ERROR) -> 
                                         C / (beta_e_checkCont (CC_meth e1 s)) ~~> C' / (beta_e_checkCont (CC_meth e1' s))                                                                       
 | beta_E_checkContract_canReduce_field : forall C e1 e1' s C',
                                          C / e1 ~~> C' / e1' ->
                                          e1' <> (beta_e_value beta_v_ERROR) ->
                                          C / (beta_e_checkCont (CC_field e1 s)) ~~> C' / (beta_e_checkCont (CC_field e1' s))                                                                        
 | beta_E_checkContract_canReduce_meth_ERR : forall C e1 s C',
                                             C / e1 ~~> C' / (beta_e_value beta_v_ERROR) ->
                                             C / (beta_e_checkCont (CC_meth e1 s)) ~~> C' / (beta_e_value beta_v_ERROR)                                                                       
 | beta_E_checkContract_canReduce_field_ERR : forall C e1 s C',
                                              C / e1 ~~> C' / (beta_e_value beta_v_ERROR) ->
                                              C / (beta_e_checkCont (CC_field e1 s)) ~~> C' / (beta_e_value beta_v_ERROR)                                                                       
 | beta_E_checkContract_NotLoc_field : forall C ev loc s C',
                                       (value (beta_s_exp ev)) ->
                                       ~(ev = (beta_e_value (beta_v_loc loc))) ->
                                       C / (beta_e_checkCont (CC_field ev s)) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_checkContract_NotLoc_meth : forall C ev loc s C',
                                       (value (beta_s_exp ev)) ->
                                       ~(ev = (beta_e_value (beta_v_loc loc))) ->
                                       C / (beta_e_checkCont (CC_meth ev s)) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_checkContract_Loc_prim_meth : forall S H X L ev loc s lp pt,
                                        (value (beta_s_exp ev)) ->
                                        (ev = (beta_e_value (beta_v_loc loc))) ->
                                        (bheap_cont_prim lp pt) = (findHeapContsAtLoc H loc) ->
                                        (bstate_state S H X L) / (beta_e_checkCont (CC_meth ev s)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_checkContract_Loc_prim_field : forall S H X L ev loc s lp pt,
                                         (value (beta_s_exp ev)) ->
                                         (ev = (beta_e_value (beta_v_loc loc))) ->
                                         (bheap_cont_prim lp pt) = (findHeapContsAtLoc H loc) ->
                                         (bstate_state S H X L) / (beta_e_checkCont (CC_field ev s)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 
 | beta_E_checkContract_Loc_NoField : forall S H X L ev loc s obj,
                                       (value (beta_s_exp ev)) ->
                                       (ev = (beta_e_value (beta_v_loc loc))) ->
                                       (bheap_cont_obj obj) = (findHeapContsAtLoc H loc) ->
                                       None = (getFieldExpFromRunObj obj s) ->
                                       (bstate_state S H X L) / (beta_e_checkCont (CC_field ev s)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_checkContract_Obj_NoMeth : forall S H X L ev obj s loc,
                                       (value (beta_s_exp ev)) ->
                                       (ev = (beta_e_value (beta_v_loc loc))) ->
                                       (bheap_cont_obj obj) = (findHeapContsAtLoc H loc) ->
                                       None = (getMethDefnFromRunObj obj s)  ->
                                       (bstate_state S H X L) / (beta_e_checkCont (CC_meth ev s)) ~~> (bstate_state S H X L) / (beta_e_value beta_v_ERROR)
 | beta_E_checkContract_Obj_Field : forall S H X L ev obj s e lts loc, 
                                    (value (beta_s_exp ev)) ->
                                    (ev = (beta_e_value (beta_v_loc loc))) ->
                                    (bheap_cont_obj obj) = (findHeapContsAtLoc H loc) ->
                                    (Some e) = (getFieldExpFromRunObj obj s) ->
                                    lts = (getFieldTypesFromRunObj obj s) ->  
                                    (bstate_state S H X L) / (beta_e_checkCont (CC_field ev s)) ~~> (bstate_state S H X L) / (applyListOfContractsFromTypeList S lts e)
 | beta_E_checkContract_Obj_Meth : forall S H X L ev obj s sTyp loc, 
                                    (value (beta_s_exp ev)) ->
                                    (ev = (beta_e_value (beta_v_loc loc))) ->
                                    (bheap_cont_obj obj) = (findHeapContsAtLoc H loc) ->
                                    sTyp = (mf_dTyp (mDecl (getMethNameTypesFromRunObj obj s)) (fDecl [])) ->  
                                    (bstate_state S H X L) / (beta_e_checkCont (CC_meth ev s)) ~~> (bstate_state S H X L) / (beta_e_contAssign (CA ev sTyp))
 
 (* (bobj_fDefn l ms (beta_fDefn fns fdcs) cs) *)
 (*| beta_E_checkContract_onObj_withField : forall C l mnds fnds cs fn,
                                          (* objContainsField (bobj_fDefn l mnds fnds cs) fn -> this is part of the contract check *)
                                          fieldContractIsSatisfied l mnds fnds cs fn ->
                                          C / (beta_s_exp (beta_e_checkCont (CC_field (beta_e_value (beta_v_runObj (bobj_fDefn l mnds fnds cs))) fn))) --> C / (beta_s_exp (beta_e_value (beta_v_runObj (bobj_fDefn l mnds fnds cs))))                                    
*)
 | beta_E_fieldAcc_canRed : forall C C' e e' f,
                            C / e ~~> C' / e' ->
                            e' <> (beta_e_value beta_v_ERROR) ->
                            C / (beta_e_fieldAcc e f) ~~> C' / (beta_e_fieldAcc e' f)
 | beta_E_fieldAcc_canRed_ERR : forall C C' e f,
                                C / e ~~> C' / (beta_e_value beta_v_ERROR) ->
                                C / (beta_e_fieldAcc e f) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_fieldAcc_notLoc : forall C ev f l C',
                            (value (beta_s_exp ev)) ->
                            ~ (ev = (beta_e_value (beta_v_loc l))) ->
                            C / (beta_e_fieldAcc ev f) ~~> C' / (beta_e_value beta_v_ERROR)
 | beta_E_fieldAcc_LocPrim : forall S H X L ev f l C' pt p,
                             (value (beta_s_exp ev)) ->
                             (ev = (beta_e_value (beta_v_loc l))) ->
                             (bheap_cont_prim p pt) = (findHeapContsAtLoc H l) ->
                             (bstate_state S H X L) / (beta_e_fieldAcc ev f) ~~> C' / (beta_e_value beta_v_ERROR)        
 (* other 2 are SProj_vthere and v_notthere*)
with ListReduction : beta_state * (list beta_exp) -> beta_state * (list beta_exp) -> Prop :=
  | LR_one : forall C C' e e' Es,
             C / e ~~> C' / e' ->
             ListReduction (C, (e :: Es)) (C', (e' :: Es))
  | LR_cons : forall C C' e Es Es',
              ListReduction (C, Es) (C', Es') ->
              ListReduction (C, (e :: Es)) (C', (e :: Es'))                                       

where "e1 '/' st1 '~~>' e2 '/' st2" := (reduction_exps (e1,st1) (e2,st2)).

Hint Constructors reduction ListReduction reduction_exps.


(* skipping the end of the reduction rules for now, still have to think about the runtime obj thing *)


(* probably dont need these anymore *)
Fixpoint InClassTable_name (cn : className) (L : list className) : Prop :=
  match L with
  | nil => False
  | x :: L1 => match (beq_cName x cn) with
               | true => True
               | false => InClassTable_name cn L1
               end
  end.

Fixpoint InInterTable_name (inm : interName) (L : list interName) : Prop :=
  match L with
  | nil => False
  | x :: L1 => match (beq_iName inm x) with
               | true => True
               | false => InInterTable_name inm L1
               end
  end.
  
(*Definition TypeInNomTable (t: ty) (Lc: list className) (Li: list interName) : Prop :=
  match t with 
  | inter_dTyp inm => InInterTable_name inm Li
  | class_dTyp cn => InClassTable_name cn Lc
  | _ => False
  end.*)

Definition isPrimType (t : ty) : Prop :=
  match t with
  | prim_dTyp p => True
  | _ => False
  end.

(*
Definition isPrimTypeProp (t : ty) : Prop :=
  match (isPrimType t) with 
  | true  => True
  | false => False
  end.*)


(* this is trash now bc of the signature *)
Notation ClassTable := (list className).

Parameter CT : ClassTable.

Notation InterTable := (list interName).

Parameter IT : InterTable.

Definition isNominal (t: ty) : Prop := 
  match t with
  | inter_dTyp inm => True
  | class_dTyp cnm => True
  | _ => False
  end.
  
Fixpoint hierarchyClasses (cd : beta_classDefn) : (list className) :=
  match cd with 
  (* base case *)
  | beta_baseClass => []
  | (beta_classBody cn1 cd1 iDs m fD) => (cn1) :: (hierarchyClasses cd1) 
  end.
  
(* omg *)
Fixpoint reduceListDim {X : Type} (l : (list (list X))) : (list X) := 
  match l with 
  | [] => []
  | L :: bigL => L ++ (reduceListDim bigL)
  end.
  
Fixpoint hierarchyInterfaces (id : interDefn) : (list interName) :=
  match id with 
  (* base case *)
  | baseInter => []
  | (interBody in1 iDs m f) => [(in1)] ++ (reduceListDim (map hierarchyInterfaces iDs)) 
  end.
  
Definition class_interfaceHierarchy (cd : beta_classDefn) : (list interName) :=
  match cd with
  | beta_baseClass => []
  | (beta_classBody cn1 cd1 iDs m fD) => (reduceListDim (map hierarchyInterfaces iDs))
  end. 
  
(* this is a list of className, interface hierarchy pairs 
   the name is paired with the interface hierarchy of the list of interfaces it implements
*)
(* adding an error state: if the class has been seen before in the hierarchy, then return empty list
   empty list means error for sure, b/c at least the class's own name should be in the list
*)

(* eyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy *)
Fixpoint inList_bool {A: Type} (a: A) (l: list A) (beqA: A -> A -> bool) : bool :=
  match l with
  | [] => false
  | b :: m => (beqA a b) || (inList_bool a m beqA)
  end.

(* probably dont need this tbh *)
(* Fixpoint totalHierarchyClasses (cd : classDefn) (seenL : list className) : (list (className * (list interName))) :=
  match cd with 
  (* base case *)
  | baseClass => [] (* should i have a default class for base class? *)
  | (classBody cn1 cd1 iDs m fD) => match (inList_bool cn1 seenL beq_cName) with 
                                         | true => []
                                         | _ => (((cn1), (reduceListDim (map hierarchyInterfaces iDs))) :: (totalHierarchyClasses cd1 (cn1 :: seenL))) 
                                         end
  end. *)

Definition typeToClassName (t: ty) : option className :=
  match t with
  | class_dTyp cn => Some cn
  | _ => None
  end.
  
Definition typeToInterName (t: ty) : option interName :=
  match t with
  | inter_dTyp inm => Some inm
  | _ => None
  end.
  
Definition typeToStruct (t: ty) : option (methDecl * fieldDecl) :=
  match t with
  | mf_dTyp Ms Fs => (Some (Ms, Fs))
  | _ => None
  end.

(* Fixpoint toStruct *)

(* helper function to finding the type of a particular field in an object *)
Fixpoint fieldAndTypeInFieldNameTypeList (fN : fieldName) (t : ty) (fnts : list (fieldName * ty)) : Prop :=
  match fnts with
  | [] => False
  | (fn1, ft1) :: Fnt  => match (fName_type_eq_dec (fN, t) (fn1, ft1)) with
                      | left _ => True
                      | right _ => (fieldAndTypeInFieldNameTypeList fN t Fnt)
                      end
  end.
  

Definition fieldAndTypeInFieldDecl (fN : fieldName) (t : ty) (fd : option fieldDecl) : Prop :=
  match fd with
  | None => False
  | Some (fDecl flst) => fieldAndTypeInFieldNameTypeList fN t flst
  end.

Definition fieldAndTypeInFieldDefn (fN : fieldName) (t : ty) (fd : beta_fieldDefn) : Prop :=
  match fd with
  | beta_fDefn fNTs fEs => fieldAndTypeInFieldNameTypeList fN t fNTs
  end.
  
Definition fieldAndTypeInObj (fN : fieldName) (t : ty) (O : beta_objRep) : Prop :=
  match O with
  | nulObj => False
  (*| bobj_fDecl l mDs fd cs => fieldAndTypeInFieldDecl fN t fd*)
  | bobj_fDefn l mDs fd cs => fieldAndTypeInFieldDefn fN t fd
  end.  
  
(* this should be almost the same as for fields, but for methods and functions *) 
(* functions to find if a particular method (name, list of types, and return type) is in an object *)
Fixpoint methNameAndTypeInMethNameTypeList (mNT : (methName * methType)) (lmnt : list (methName * methType)) : Prop :=
  match lmnt with
  | [] => False
  | (mn1, mnt1) :: M => match (mName_mType_eq_dec (mn1, mnt1) mNT) with
                        | left _ => True
                        | right _ => (methNameAndTypeInMethNameTypeList mNT M)
                        end
  end.
  
Fixpoint methTypeInMethNameTypeList (mT : methType) (lmnt : list (methName * methType)) : Prop :=
  match lmnt with
  | [] => False
  | (mn1, mnt1) :: M => match (mType_eq_dec mnt1 mT) with
                        | left _ => True
                        | right _ => (methTypeInMethNameTypeList mT M)
                        end
  end.

Definition methNameAndTypeInMethDefn (mNT : (methName * methType)) (mDs : beta_methDefn) : Prop :=
  match mDs with
  | beta_mDefn lNTs lCs => methNameAndTypeInMethNameTypeList mNT lNTs
  end.
  
Definition methTypeInMethDefn (mT : methType) (mDs : beta_methDefn) : Prop :=
  match mDs with
  | beta_mDefn lNTs lCs => methTypeInMethNameTypeList mT lNTs
  end.
  
Definition methTypeInMethDecl (mT: methType) (mDs: methDecl) : Prop :=
  match mDs with 
  | mDecl lmnts => (methTypeInMethNameTypeList mT lmnts)
  end.
  
Definition methNameAndTypeInMethDecl (mNT: (methName * methType)) (mDs: methDecl) : Prop :=
  match mDs with 
  | mDecl lmnts => (methNameAndTypeInMethNameTypeList mNT lmnts)
  end.

Fixpoint combineProps (lp : list Prop) : Prop :=
  match lp with
  | [] => True
  | p :: lp1 => p /\ (combineProps lp1)
  end.

Definition methNameAndTypeInObj (mNT : (methName * methType)) (O : beta_objRep) : Prop :=
  match O with
  | nulObj => False
  (*| bobj_fDecl l mDs fd cs => combineProps (List.map (methNameAndTypeInMethDecl mNT) mDs)*)
  | bobj_fDefn l mDs fd cs => combineProps (List.map (methNameAndTypeInMethDefn mNT) mDs)
  end.
  
Definition methTypeInObj (mT : methType) (O : beta_objRep) : Prop :=
  match O with
  | nulObj => False
  (*| bobj_fDecl l mDs fd cs => combineProps (List.map (methTypeInMethDecl mT) mDs)*)
  | bobj_fDefn l mDs fd cs => combineProps (List.map (methTypeInMethDefn mT) mDs)
  end.
  
Definition getMethNameTypeListFromDecl (mdcl: methDecl) : list (methName * methType) :=
  match mdcl with
  | mDecl lmnts => lmnts
  end.

Definition getMethNameTypeListFromType (S: signature) (t : ty) : list (methName * methType) :=
  match t with
  | mf_dTyp mds fds => (getMethNameTypeListFromDecl mds)
  | class_dTyp cN => (getMethNameTypeListFromDecl (getMethDeclFromMethDeclList (getMethDeclListFromDefnList (getMethodList (getMethods (getClass S cN))))))
  | _ => []
  end.

         
(* for funct obls, this is when the object is a function, so there should not be a name (?) *)             
(*Definition check_the_contract_obj (c : beta_obls) (O : beta_objRep) : Prop :=
  match c with
  | nominal_obl cN => True (* how do you even check this *)
  | field_obl fN t => fieldAndTypeInObj fN t O
  | funct_obl mT => methTypeInObj mT O
  | meth_obl mNT => methNameAndTypeInObj mNT O
  | prim_obl primType => False (* since this is an object, can't satisfy primitive *)
  end.*)
  
(* what are we gonna need in terms of functions over the contracts....
   we need contract_assign and contract_check... but these need to be compiled into shit that runs during
   the runtime stage of the compilation. 
   
   contract_check -- takes expression (which should resolve to an object), and type to check with, and fieldName to check over
   contract_assign -- takes expression (which should be an object or a value), and a type to add a contract for. should check existing contracts?

*)

(* returns True if L1 is a subset of L2 *)
Fixpoint subset_list_fDecl (L1 L2: (list (fieldName * ty))): Prop :=
  match L1 with
  | [] => True (* b/c have reached the end of the list without exiting as False *)
  | ((fn, t) :: L) => match (in_dec fName_type_eq_dec (fn, t) L2) with 
              | left _ => (subset_list_fDecl L L2)
              | right _ => False
              end
  end.
  
Fixpoint subset_list_mDecl (L1 L2: (list (methName * methType))): Prop :=
  match L1 with
  | [] => True (* b/c have reached the end of the list without exiting as False *)
  | ((mn, t) :: L) => match (in_dec mName_mType_eq_dec (mn, t) L2) with 
              | left _ => (subset_list_mDecl L L2)
              | right _ => False
              end
  end.

(* Subtyping judgement -- don't need subtyping difference anymore, so don't bother calculating it *)
Inductive subtyping : signature -> ty -> ty -> Prop :=
  | S_Transi : forall S t1 t2 t3,
               subtyping S t1 t2 ->
               subtyping S t2 t3 ->
               subtyping S t1 t3
  | S_Refl : forall S tau,
             subtyping S tau tau 
 (* | S_NoMany : forall tau,
               (isPrimType tau) \/ (TypeInNomTable tau CT IT) ->
               subtyping tau Any *)
  | S_Void : forall S tau,
               subtyping S (prim_dTyp prim_Void) tau 
  (*| S_StAny : forall M F, 
              subtyping (mf_dTyp M F) Any*)
  (* this is S_NoMany and S_StAny combined *)
  | S_Any : forall S tau,
            subtyping S tau Any
  (*| S_NStruct : forall t M F,
                (InClassTable t CT) \/ (InInterTable t IT) ->  *)
  | S_Nom_CC : forall S t1 t2 cd1 cn1 cn2 hier,
               (Some cn1) = (typeToClassName t1) ->
               (Some cn2) = (typeToClassName t2) ->
               (Some cd1) = (getClass S cn1) ->
               hier = (hierarchyClasses cd1) ->
               ((inList_bool cn2 hier beq_cName) = true) -> 
               subtyping S t1 t2 
  | S_Nom_CI : forall S t1 t2 cn1 cd1 in2 hier,
               (Some cn1) = (typeToClassName t1) ->
               (Some in2) = (typeToInterName t2) ->
               (Some cd1) = (getClass S cn1) ->
               hier = (class_interfaceHierarchy cd1) ->
               ((inList_bool in2 hier beq_iName) = true) -> 
               subtyping S t1 t2 
  (* S_Nom_IC not possible bc an interface cannot be a subtype of a class *)      
  | S_Nom_II : forall S t1 t2 id1 in1 in2 hier,
               (Some in1) = (typeToInterName t1) ->
               (Some in2) = (typeToInterName t2) ->
               (Some id1) = (getInterface S in1) ->
               hier = (hierarchyInterfaces id1) ->
               ((inList_bool in2 hier beq_iName) = true) -> 
               subtyping S t1 t2        
  | S_Rec : forall S t1 t2 lmnts1 lfnts1 lmnts2 lfnts2,
            (Some ((mDecl lmnts1), (fDecl lfnts1))) = (typeToStruct t1) -> 
            (Some ((mDecl lmnts2), (fDecl lfnts2))) = (typeToStruct t2) -> 
            (subset_list_fDecl lfnts1 lfnts2) ->
            (subset_list_mDecl lmnts1 lmnts2) ->
            subtyping S t1 t2
  
  with methDecl_subTyping : signature -> methDecl -> methDecl -> Prop :=
  | S_MethDeclEmpty : forall S mD2 mD1 ml1,
                      (mDecl ml1) = mD1 ->
                      ml1 = [] -> 
                      methDecl_subTyping S mD1 mD2 
  | S_MethDecls : forall S mD1 ml1 mD2 ml2 m1,
                  (mDecl (m1 :: ml1)) = mD1 -> 
                  (mDecl ml2) = mD2 ->
                  
                  (* for m1, exists some m in ml2 that satisfies methType_subTyping S m1 m *)
                  (exists m, ((In m ml2) /\ (methType_subTyping S m1 m))) ->
                  methDecl_subTyping S mD1 mD2
  
  (* subtyping for method types *)
  with methType_subTyping : signature -> (methName * methType) -> (methName * methType) -> Prop :=
  | S_MethType : forall S mt1 mt2 lt1 lt2 t1 t2 mn1 mn2,
                 (mn1, (methType_ty lt1 t1)) = mt1 ->
                 (mn2, (methType_ty lt2 t2)) = mt2 ->
                 subtyping S t1 t2 -> (* return type is t1 <: t2 to get mt1 <: mt2 *)
                 typeList_subTyping S lt2 lt1 ->
                 methType_subTyping S mt1 mt2
  with typeList_subTyping : signature -> list ty -> list ty -> Prop :=
  | S_ListEmpty : forall S,
                  typeList_subTyping S [] []
  | S_Lists     : forall S t1 lt1 t2 lt2,
                  subtyping S t1 t2 ->
                  typeList_subTyping S (t1 :: lt1) (t2 :: lt2)              
  .
  
Hint Constructors subtyping methDecl_subTyping methType_subTyping.
  
Fixpoint env_typeAtVar (Gamma: typeEnv) (x: var) : option ty :=
  match Gamma with
  | [] => None
  | (x1, t1) :: L => match (beq_var x1 x) with
                     | true => (Some t1)
                     | _ => (env_typeAtVar L x)
                     end
  end. 
 
Definition getMethDeclFromDefn_exp (mD: methDefn) : methDecl :=
  match mD with
  mDefn lmnts lmcs => mDecl lmnts (* (fst (split lmnts)) (snd (split lmnts))*)
  end.
  
Definition getFieldDeclFromDefn_exp (fD: fieldDefn) : fieldDecl :=
  match fD with
  fDefn lfnts lfcs => fDecl lfnts (*(fst (split lfnts)) (snd (split lfnts))*)
  end. 
    
(* mDefn : list (methName * methType) -> list methDef_conts -> methDefn *)
(* fDefn : list (fieldName * ty) -> list exp -> fieldDefn *)
(* mf_dTyp : methDecl -> fieldDecl -> ty *)
Definition getStructTypeFromETheDefns (md: methDefn) (fd: fieldDefn) : ty :=
  mf_dTyp (getMethDeclFromDefn_exp md) (getFieldDeclFromDefn_exp fd).
  
(* using this name as it's the exact auxilliary function as named in the STS paper *)(*
Definition sig (tp : (methDefn * fieldDefn)) : ty :=
  (mf_dTyp (fst (getStructTypeFromDefnPair tp)) (snd (getStructTypeFromDefnPair tp))).
*)  
(* Expression typing under context bigTau *)
(* This is getting rid of types on exps *)
(* Gonna have to add auxiliary functions to exp probably to be able to add them here in the rules *)  

Fixpoint getFieldTypeFromFNTList (f : fieldName) (lfnts : list (fieldName * ty)) : option ty :=
  match lfnts with
  | [] => None
  | (f1, t1) :: rfnts => match (beq_fName f1 f) with
                         | true => Some t1
                         | false => getFieldTypeFromFNTList f rfnts
                         end 
  end.

Definition getFieldTypeFromDecl (f : fieldName) (fd : option fieldDecl) : option ty :=
  match fd with
  | None => None
  | Some (fDecl lfnts) => getFieldTypeFromFNTList f lfnts
  end.

Definition typeOfFieldInType (f : fieldName) (t : ty) (S : signature) : option ty := 
  match t with
  | mf_dTyp md fd => getFieldTypeFromDecl f (Some fd) 
  | class_dTyp cn => getFieldTypeFromDecl f (getFields (getClass S cn))
  | _ => None
  end.

Close Scope string_scope.

Fixpoint contractAssignForList (lexps: list beta_exp) (lts: list ty) : list contractAssign :=
  match lexps, lts with
  | [], _ => []
  | _, [] => []
  | e :: rlexps, t :: rlts => (CA e t) :: (contractAssignForList rlexps rlts)
  end.
  
(*Fixpoint getLocTypeFromStoreTyping (st : store_typing) (l : location) : option ty :=
  match st with
  | [] => None
  | (l1, t1) :: rst => match (beq_loc l l1) with
                       | true => Some t1
                       | false => getLocTypeFromStoreTyping rst l
                       end 
  end.*)
Definition getLocTypeFromStoreTyping (st : store_typing) (l : location) : option ty :=
  match l with
  | nulLoc => None
  | thisLoc n => nth_error st n
  end.
  
Fixpoint typedcontractAssignOverList_lfnts_exps (lfnts : list (fieldName * ty)) (es : list exp) : list exp :=
  match lfnts, es with
  | [], [] => []
  | [], _ => []
  | _, [] => []
  | ((fn, ft) :: rlfnts), (e :: res) => (e_contAssign (t_CA e ft)) :: (typedcontractAssignOverList_lfnts_exps rlfnts res)
  end.
  
(* Definition getFNTListFromType (S : signature) (t : ty) : list (fieldName * ty) :=
  match t with
  | class_dTyp cn => getFNTListFromFieldDeclList (getFieldDeclFromOptionDecl (getFields (getClass S cN))) *)
  
Inductive env_ok : typeEnv -> Prop :=
  | ok_empty : env_ok []
  | ok_extend : forall Gamma v t,
                env_ok Gamma -> 
                (forall (t' : ty), ~In (v,t') Gamma) ->
                env_ok ((v,t)::Gamma).
                
Inductive ST_Extends : store_typing -> store_typing -> Prop :=
  | stextend_nil : forall Delta', ST_Extends Delta' []
  | stextend_cons : forall Delta Delta' x,
                    ST_Extends Delta' Delta ->
                    ST_Extends (x :: Delta') (x :: Delta).
                  
Inductive Sig_Extends : signature -> signature -> Prop :=
  | sigextends_nil : forall S', Sig_Extends S' sig_null
  | sigextend_id : forall S S' id,
                   Sig_Extends S' S ->
                   Sig_Extends (sig_interDef S' id) (sig_interDef S id)
  | sigextend_cd : forall S S' cd,
                   Sig_Extends S' S ->
                   Sig_Extends (sig_classDef S' cd) (sig_classDef S cd).

Hint Constructors ST_Extends Sig_Extends.

Fixpoint fieldInNameTypeListHasType (f : fieldName) (lfnts : list (fieldName * ty)) : option ty :=
  match lfnts with
  | [] => None
  | (fn, ft) :: rfnts => match (beq_fName fn f) with
                         | true => Some ft
                         | false => (fieldInNameTypeListHasType f rfnts)
                         end 
  end.

Definition fieldInDeclHasType (f : fieldName) (ofd : option fieldDecl) : option ty :=
  match ofd with
  | None => None
  | (Some (fDecl lfnts)) => (fieldInNameTypeListHasType f lfnts)
  end.

(*(fieldInTypeHasType (Field f) t) = (Some tf)*)
Definition fieldInTypeHasType (f : fieldName) (t : ty) (S : signature) : option ty := 
  match t with
  | class_dTyp cn => (fieldInDeclHasType f (getFields (getClass S cn)))
  | mf_dTyp md fd => (fieldInDeclHasType f (Some fd))
  | _ => None
  end. 

(* Add typing rules for all the expressions in the language, even those which make no sense in the typed language 
   -- they must exist formally so that preservation can be proven *)  

Inductive type_reduction_exps : typContext -> (exp * ty) -> beta_exp -> Prop :=
  | T_Loc : forall S Sigma Gamma l t,
            (getLocTypeFromStoreTyping Sigma l) = (Some t) ->
            type_reduction_exps (typConx S Sigma Gamma) ((e_value (v_locc l)), t) (beta_e_value (beta_v_loc l))
  | T_Env : forall S Sigma Gamma x t,
            (Some t) = (env_typeAtVar Gamma x) ->
            type_reduction_exps (typConx S Sigma Gamma) ((e_var x), t) (beta_e_var x)
  | T_Const : forall S Sigma Gamma l pt,
              (*(subtyping S (prim_dTyp pt) t) ->*)
              type_reduction_exps (typConx S Sigma Gamma) ((e_value (v_lit_prim l pt)), (prim_dTyp pt)) (beta_e_value (beta_v_lit_prim l pt))
  | T_Rec : forall Tau Ms lfnts lfes Ms' lfes',
            type_reduction_fieldDefns Tau (fDefn lfnts lfes) (beta_fDefn lfnts lfes') ->
            type_reduction_methDefns Tau Ms Ms' ->
            type_reduction_exps Tau ((e_FTDefns Ms (fDefn lfnts lfes)), (getStructTypeFromETheDefns Ms (fDefn lfnts lfes))) (beta_e_defFirstTR Ms' (beta_fDefn lfnts lfes'))
  | T_Rec_TRd : forall Tau Ms lfnts lfes Ms' lfes',
                type_reduction_exps_list Tau (lfes, (snd (split lfnts))) lfes' ->
                type_reduction_exps Tau ((e_theDefns Ms (fDefn lfnts lfes)), (getStructTypeFromETheDefns Ms (fDefn lfnts lfes))) (beta_e_theDefns Ms' (beta_fDefn lfnts lfes'))
  | T_New : forall S Sigma Gamma cN lexps ltprms lfnts lts lexprms Cdef1,
            type_reduction_exps_list (typConx S Sigma Gamma) (lexps, ltprms) lexprms ->
            (getClass S cN) = (Some Cdef1) ->
            (fDecl lfnts) = (getFieldDeclFromOptionDecl (getFields (Some Cdef1))) -> 
            lts = (snd (split lfnts)) ->
            typeList_subTyping S ltprms lts -> 
            type_reduction_exps (typConx S Sigma Gamma) ((e_newClass cN lexps), (class_dTyp cN)) (beta_e_contAssign (CA (beta_e_newClass cN (CA_list lexprms ltprms)) (class_dTyp cN)))
  | T_SMCall : forall S Sigma Gamma e t t' e' lexps lexps' lts' lts mN,
               type_reduction_exps (typConx S Sigma Gamma) (e, t') e' ->
               type_reduction_exps_list (typConx S Sigma Gamma) (lexps, lts') lexps' ->
               typeList_subTyping S lts' lts ->  
               (In (mN, (methType_ty lts t)) (getMethNameTypeListFromType S t')) ->
               type_reduction_exps (typConx S Sigma Gamma) ((e_methCall e mN lexps), t) (beta_e_methCall e' mN ((CA_list lexps' lts)))
  | T_ContAssign : forall S Sigma Gamma e e' t t' t'',
                   type_reduction_exps (typConx S Sigma Gamma) (e, t'') e' ->
                   subtyping S t'' t' ->
                   type_reduction_exps (typConx S Sigma Gamma) ((e_contAssign (t_CA e t)), t') (beta_e_contAssign (CA e' t))
  (* type_reduction_exps (typConx S' Sigma' []) (ee', t')
    (beta_e_obj (bobj_fDefn (thisLoc (stLength H)) [Mdef] (beta_fDefn lfnts lfes') [])) *)
  | T_ObjNotation : forall Tau lfnts lfes lfes' Ms Ms' cs cs' l,
                    type_reduction_exps_list Tau (lfes, (snd (split lfnts))) lfes' ->
                    type_reduction_exps Tau ((e_bobj l [Ms] (fDefn lfnts lfes) cs), (getStructTypeFromETheDefns Ms (fDefn lfnts lfes))) (beta_e_obj (bobj_fDefn l [Ms'] (beta_fDefn lfnts lfes') cs'))
  | T_Cast : forall S Sigma Gamma e e' t t',
             type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
             subtyping S t t' ->
             type_reduction_exps (typConx S Sigma Gamma) (e_FTcast t' e, t) (beta_e_FTcast t' (beta_e_contAssign (CA e' t)))
  | T_Cast_TR : forall S Sigma Gamma e e' t t',
                type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
                subtyping S t t' ->
                type_reduction_exps (typConx S Sigma Gamma) (e_cast t' e, t) (beta_e_cast t' e')
  | T_Check_Cont_field : forall Tau e e' t t' f,
                        type_reduction_exps Tau (e, t') e' ->
                        type_reduction_exps Tau ((e_fieldAcc e (Field f)), t) (beta_e_fieldAcc e' (Field f)) ->
                        type_reduction_exps Tau ((e_checkCont (t_CC e f)), t) (beta_e_checkCont (CC_field e' (Field f))) 
  | T_Check_Cont_meth : forall Tau e e' t t' m,
                        type_reduction_exps Tau (e, t') e' ->
                        type_reduction_exps Tau ((e_checkCont (t_CC e m)), t) (beta_e_checkCont (CC_meth e' (Method m))) 
  | T_FldRd : forall S Sigma Gamma e t e' f tf,
              type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
              (typeOfFieldInType (Field f) t S) = (Some tf) ->
              type_reduction_exps (typConx S Sigma Gamma) ((e_fieldAcc_TR e (Field f)), tf) (beta_e_checkCont (CC_field e' (Field f)))  
  | T_FldRd_TR : forall S Sigma Gamma e t e' f tf,
                 type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
                 (typeOfFieldInType (Field f) t S) = (Some tf) ->
                 type_reduction_exps (typConx S Sigma Gamma) ((e_fieldAcc e (Field f)), tf) (beta_e_fieldAcc e' (Field f))
(*  | T_FldRd_getVal : forall S Sigma Gamma e0 f t loc e H obj t0,
                     type_reduction_exps (typConx S Sigma Gamma) (e_fieldAcc e0 (Field f), t0) (beta_e_fieldAcc (beta_e_value (beta_v_loc loc)) (Field f)) ->
                     typeOfFieldInType (Field f) t0 S = Some t ->
                     bheap_cont_obj obj = findHeapContsAtLoc H loc ->
                     Some e = getFieldExpFromRunObj obj (Field f) ->
                     type_reduction_exps (typConx S Sigma Gamma) ((e_fieldAcc (e_value (v_locc loc)) (Field f)), t) (applyListOfContractsFromTypeList S (getFieldTypesFromRunObj obj (Field f)) e)
*)with type_reduction_exps_list : typContext -> ((list exp) * (list ty)) -> list beta_exp -> Prop :=
  | T_ListExp_Empty : forall tC,
                      type_reduction_exps_list tC ([], []) []
  | T_ListExp_Cons  : forall S Sigma Gamma e eL t tL be beL t',
                      subtyping S t' t -> 
                      type_reduction_exps (typConx S Sigma Gamma) (e, t') be ->
                      type_reduction_exps_list (typConx S Sigma Gamma) (eL, tL) beL ->
                      type_reduction_exps_list (typConx S Sigma Gamma) (e :: eL, t :: tL) (be :: beL)
  
                      (*forall tC e eL t tL be beL,
                      type_reduction_exps tC (e, t) be ->
                      type_reduction_exps_list tC (eL, tL) beL ->
                      type_reduction_exps_list tC (e :: eL, t :: tL) (be :: beL)*)
(*with type_reduction_list : typContext -> list stat -> list beta_stat -> Prop :=
  | T_List_Empty : forall tC,
                      type_reduction_list tC [] []
  | T_List_Cons  : forall tC s sL bs bsL,
                      type_reduction tC s bs ->
                      type_reduction_list tC sL bsL ->
                      type_reduction_list tC (s :: sL) (bs :: bsL)*)
  
with type_reduction_fieldDefns : typContext -> fieldDefn -> beta_fieldDefn -> Prop :=
  | T_FDs_Empty : forall tC, 
                  type_reduction_fieldDefns tC (fDefn [] []) (beta_fDefn [] []) 
  | T_FDs_Cons  : forall S Sigma Gamma e1 e1' t1' t1 fn lfes lfnts lfes',
                  type_reduction_exps (typConx S Sigma Gamma) (e1, t1') e1' ->
                  subtyping S t1' t1 ->
                  type_reduction_fieldDefns (typConx S Sigma Gamma) (fDefn lfnts lfes) (beta_fDefn lfnts lfes') ->
                  type_reduction_fieldDefns (typConx S Sigma Gamma) (fDefn ((fn, t1) :: lfnts) (e1 :: lfes)) (beta_fDefn ((fn, t1) :: lfnts) ((beta_e_contAssign (CA e1' t1)) :: lfes'))
with type_reduction_methDefns : typContext -> methDefn -> beta_methDefn -> Prop :=
  | T_MDs_Empty : forall tC,
                  type_reduction_methDefns tC (mDefn [] []) (beta_mDefn [] []) 
(* beta_methDef_conts : Type := beta_mD_conts : list (var * ty) -> ty -> beta_stat -> beta_exp -> beta_methDef_conts *)
  | T_MDs_Cons  : forall S Sigma curGammaList mnt1 lmnts (lvt1: list (var * ty) ) t1 s1 e1 s1' e1' lmcts lmcts',
                  (*curGammaList' = curGammaList ++ lvt1 ->*)
                  type_reduction (typConx S Sigma (curGammaList ++ lvt1)) s1 s1' ->
                  type_reduction_exps (typConx S Sigma (curGammaList ++ lvt1)) (e1, t1) e1' ->
                  type_reduction_methDefns (typConx S Sigma curGammaList) (mDefn lmnts lmcts) (beta_mDefn lmnts lmcts') ->
                  type_reduction_methDefns (typConx S Sigma curGammaList) (mDefn (mnt1 :: lmnts) ((mD_conts lvt1 t1 s1 e1) :: lmcts)) (beta_mDefn (mnt1 :: lmnts) ((beta_mD_conts lvt1 t1 s1' e1') :: lmcts'))

(* Need some concept of T |- e : tau -> e' *)  
with type_reduction : typContext -> stat -> beta_stat -> Prop :=
  | T_Exp : forall tC e e' t,
            type_reduction_exps tC (e, t) e' ->
            type_reduction tC (s_exp e) (beta_s_exp e')
  | T_Skip : forall tC,
             type_reduction tC s_skip beta_s_skip
  | T_VDef : forall S Sigma Gamma x t e e' t',
             type_reduction_exps (typConx S Sigma Gamma) (e, t') e' ->
             e' <> (beta_e_value beta_v_ERROR) ->
             subtyping S t' t ->
             type_reduction (typConx S Sigma Gamma) (s_varAssign x t e) (beta_s_varAssign x t e') (*(beta_s_varAssign x t (beta_e_contAssign (CA e' t')))*)
  | T_VAssign : forall S Sigma Gamma x t e t' e',
                type_reduction_exps (typConx S Sigma Gamma) ((e_var x), t) (beta_e_var x) -> 
                type_reduction_exps (typConx S Sigma Gamma) (e, t') e' ->
                subtyping S t' t ->
                type_reduction (typConx S Sigma Gamma) (s_assignNoVar x e) (beta_s_assignNoVar x e') (*(beta_s_assignNoVar x (beta_e_contAssign (CA e' t')))*)  
  (* T_SFAssign and T_DFAssign*)
  | T_SFAssign : forall S Sigma Gamma e1 t1 e1' e2 t2 e2' f,
                 type_reduction_exps (typConx S Sigma Gamma) (e1, t1) e1' ->
                 type_reduction_exps (typConx S Sigma Gamma) (e2, t2) e2' ->
                 (*(Some t) = (typeOfFieldInType f t1 S) ->*)
                 subtyping S t1 t2 -> (* structurally i think this works *)
                 (* dont' need to check if e1.f actually exists since this is done at runtime *)
                 type_reduction (typConx S Sigma Gamma) (s_fieldAssign e1 f e2) (beta_s_fieldAssign e1' f e2')
  (*| T_DFAssign : forall tC e1 e2 e3 t1 t2 t3 e1' e2' e3',
                 type_reduction_exps tC (e1, t1) e1' -> 
                 type_reduction_exps tC (e2, t2) e2' ->
                 type_reduction_exps tC (e3, t3) e3' ->
                 type_reduction tC (s_accAssign e1 e2 e3) (beta_s_accAssign e1' e2' e3') *)
  | T_If : forall tC e e' s1 s2 s1' s2' t,
           type_reduction_exps tC (e, t) e' ->
           type_reduction tC s1 s1' ->
           type_reduction tC s2 s2' ->
           type_reduction tC (s_ifElse e s1 s2) (beta_s_ifElse e' s1' s2')
  | T_Seq : forall tC s1 s1' s2 s2', 
            type_reduction tC s1 s1' ->
            type_reduction tC s2 s2' ->
            type_reduction tC (s_stats s1 s2) (beta_s_stats s1' s2')
  .

Hint Constructors type_reduction type_reduction_exps type_reduction_exps_list type_reduction_methDefns type_reduction_fieldDefns.

Parameter prim_ty_eq_dec : forall p1 p2 : primType, {p1 = p2} + {p1 <> p2}.
Parameter expListValues_eq_dec : forall lexp: list beta_exp, {expListAllValues lexp} + {~ (expListAllValues lexp)}.
Parameter errorInListOfExps_eq_dec : forall lexp: list beta_exp, {errorInListOfExps lexp} + {~ (errorInListOfExps lexp)}.

(*
Lemma contractAssignCanStep: forall C e tau,
      exists C' e', (reduction (C, (beta_s_exp (beta_e_contAssign (CA e tau)))) (C', (beta_s_exp e'))).
Proof. 
*)

Scheme typing_s_ind := Minimality for type_reduction Sort Prop 
with typing_e_ind := Minimality for type_reduction_exps Sort Prop
with typing_es_ind := Minimality for type_reduction_exps_list Sort Prop
with typing_f_ind := Minimality for type_reduction_fieldDefns Sort Prop
(*with typing_ss_ind : Minimality for type_reduction_list Sort Prop*)
with typing_m_ind := Minimality for type_reduction_methDefns Sort Prop.

Combined Scheme typing_mutind from typing_s_ind, typing_e_ind, typing_es_ind, typing_f_ind, typing_m_ind (*typing_ss_ind*).

Ltac ObjLitCanRed := econstructor; econstructor; eapply beta_E_ObjLit_canReduce; eapply LR_one.

Ltac var_econ n :=
  match n with
  | 0 => idtac
  | S ?n' => econstructor; var_econ n'
  end.
  
Lemma ifObjSubtypeObjType : 
  forall S oT t,
    (isObjType oT) /\ (subtyping S oT t) ->
    (isObjType t).
Proof.
  intros. inversion H.
  induction H1.
  - apply IHsubtyping2; eauto. 
  - destruct tau; eauto.
  - inversion H0.
  - unfold isObjType. trivial.
  - destruct t2; try (inversion H2; fail). eauto.
  - destruct t2; try (inversion H2; fail). eauto.
  - destruct t2; try (inversion H2; fail). eauto.
  - destruct t2; try (inversion H2; fail). eauto.
Qed.

Lemma heap_extends_lookup : forall n HT HT' t,
  (Some t) = (getLocTypeFromStoreTyping HT (thisLoc n)) ->
  ST_Extends HT' HT ->
  (Some t) = (getLocTypeFromStoreTyping HT' (thisLoc n)).
Proof.
  intros. generalize dependent n. dependent induction HT; intros.
  - simpl in H; destruct n; inversion H.
  - destruct HT'.
    + inversion H0.
    + inversion H0; subst.
      destruct n.
      * simpl in *. eauto.
      * simpl in *. eapply IHHT in H; eauto.
Qed.

Lemma sig_extends_getMethNameTypeListFromType : forall S S' t lmnt,
  lmnt = getMethNameTypeListFromType S t ->
  Sig_Extends S' S ->
  lmnt = getMethNameTypeListFromType S' t.
Proof. 
  intros. generalize dependent t. dependent induction S; intros; admit.
Admitted.  

Lemma sig_extends_lookup_in : forall iN S S' iD,
  (Some iD) = (getInterface S iN) ->
  Sig_Extends S' S ->
  (Some iD) = (getInterface S' iN).
Proof. 
  intros. induction H0. 
  - subst. inversion H. 
  - destruct id0.
    + simpl in H. eapply IHSig_Extends in H. simpl. auto.
    + simpl in *. destruct (beq_iName iN i); eauto.
  - inversion H.
Qed.

Lemma sig_extends_lookup_cn : forall cN S S' cD,
  (Some cD) = (getClass S cN) ->
  Sig_Extends S' S ->
  (Some cD) = (getClass S' cN).
Proof.
  intros. induction H0. 
  - subst. inversion H.
  - simpl in *. eauto. 
  - destruct cd. 
    + simpl in H. eapply IHSig_Extends in H. simpl. eauto.
    + simpl in *. destruct (beq_cName cN c); eauto.
Qed. 

Lemma sig_extends_getClass : forall S S' cN cD,
  (Some cD) = (getClass S cN) ->
  Sig_Extends S' S ->
  (Some cD) = (getClass S' cN).
Proof.
  intros. induction H0.
  - inversion H. 
  - simpl in *. eauto. 
  - destruct cd. 
    + simpl in H. eapply IHSig_Extends. eauto. 
    + simpl in *. destruct (beq_cName cN c); eauto. 
Qed. 

Lemma sig_extends_getInterface : forall S S' iN iD,
  (Some iD) = (getInterface S iN) ->
  Sig_Extends S' S ->
  (Some iD) = (getInterface S' iN).
Proof.
  intros. induction H0. 
  - inversion H.
  - destruct id0.
    + simpl in H. eapply IHSig_Extends in H. simpl. auto.
    + simpl in *. destruct (beq_iName iN i); eauto.
  - inversion H.
Qed.

Lemma sig_extends_subtyping : forall S S' t t',
  subtyping S t' t ->
  Sig_Extends S' S ->
  subtyping S' t' t.
Proof.
  intros.
  subst. induction H; eauto.
  - eapply S_Nom_CC; eauto. eapply sig_extends_getClass; eauto.
  - eapply S_Nom_CI; eauto. eapply sig_extends_getClass; eauto.
  - eapply S_Nom_II; eauto. eapply sig_extends_getInterface; eauto. 
Qed.

Lemma sig_extends_typeListSubtyping : forall S S' lts lts',
  typeList_subTyping S lts' lts ->
  Sig_Extends S' S ->
  typeList_subTyping S' lts' lts.
Proof.
  intros.
  subst. induction H; eauto.
  - eapply S_ListEmpty.
  - eapply S_Lists. eapply sig_extends_subtyping; eauto. 
Qed. 

Lemma ST_extends_app : forall Sigma lt,
  ST_Extends (Sigma ++ [lt]) Sigma.
Proof.
  intros. induction Sigma; intros; eauto.
  simpl. eauto.
Qed. 

Lemma sig_weakening :  
  (forall S Sigma Gamma ss s, type_reduction (typConx S Sigma Gamma) ss s ->
    (forall S', Sig_Extends S' S -> type_reduction (typConx S' Sigma Gamma) ss s)) /\
  (forall S Sigma Gamma e t e', type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
    (forall S', Sig_Extends S' S -> type_reduction_exps (typConx S' Sigma Gamma) (e, t) e')) /\
  (forall S Sigma Gamma es ts es', type_reduction_exps_list (typConx S Sigma Gamma) (es, ts) es' ->
    (forall S', Sig_Extends S' S -> type_reduction_exps_list (typConx S' Sigma Gamma) (es, ts) es')) /\
  (forall S Sigma Gamma fd fd', type_reduction_fieldDefns (typConx S Sigma Gamma) fd fd' ->
    (forall S', Sig_Extends S' S -> type_reduction_fieldDefns (typConx S' Sigma Gamma) fd fd')) /\
  (forall S Sigma Gamma md md', type_reduction_methDefns (typConx S Sigma Gamma) md md' ->
    (forall S', Sig_Extends S' S -> type_reduction_methDefns (typConx S' Sigma Gamma) md md')).
Proof.
  eapply typing_mutind; intros.
  - eapply T_Exp. eauto. 
  - eapply T_Skip. 
  - eapply T_VDef; eauto.
  - eapply T_VAssign; eauto.
  - eapply T_SFAssign; eauto.
  - eapply T_If; eauto. 
  - eapply T_Seq; eauto.
  - eapply T_Loc; eauto. 
  - eapply T_Env; eauto.
  - eapply T_Const; eauto. 
  - eapply T_Rec; eauto. 
  - eapply T_Rec_TRd; eauto. 
  - eapply T_New; eauto. 
  - eapply T_SMCall; eauto. 
  - eapply T_ContAssign; eauto. 
  - eapply T_ObjNotation; eauto. 
  - eapply T_Cast; eauto. 
  - eapply T_Cast_TR; eauto. 
  - eapply T_Check_Cont_field; eauto. 
  - eapply T_Check_Cont_meth; eauto. 
  - eapply T_FldRd; eauto.
  - eapply T_FldRd_TR; eauto.  
  (* - eapply T_FldRd_getVal; eauto.  *) 
  - eapply T_ListExp_Empty; eauto.
  - eapply T_ListExp_Cons; eauto. 
  - eapply T_FDs_Empty; eauto. 
  - eapply T_FDs_Cons; eauto. 
  - intros. split; intros. 
    + admit.
    + split; intros; admit. 
  - eauto. 
  - eauto. 
Admitted.   
      
Lemma sigma_weakening : 
  (forall S Sigma Gamma ss s, type_reduction (typConx S Sigma Gamma) ss s ->
    (forall Sigma', ST_Extends Sigma' Sigma -> type_reduction (typConx S Sigma' Gamma) ss s)) /\
  (forall S Sigma Gamma e t e', type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
    (forall Sigma', ST_Extends Sigma' Sigma -> type_reduction_exps (typConx S Sigma' Gamma) (e, t) e')) /\
  (forall S Sigma Gamma es ts es', type_reduction_exps_list (typConx S Sigma Gamma) (es, ts) es' ->
    (forall Sigma', ST_Extends Sigma' Sigma -> type_reduction_exps_list (typConx S Sigma' Gamma) (es, ts) es')) /\
  (forall S Sigma Gamma fd fd', type_reduction_fieldDefns (typConx S Sigma Gamma) fd fd' ->
    (forall Sigma', ST_Extends Sigma' Sigma -> type_reduction_fieldDefns (typConx S Sigma' Gamma) fd fd')) /\
  (forall S Sigma Gamma md md', type_reduction_methDefns (typConx S Sigma Gamma) md md' ->
    (forall Sigma', ST_Extends Sigma' Sigma -> type_reduction_methDefns (typConx S Sigma' Gamma) md md')).
Proof.
  eapply typing_mutind; intros.
  - eapply T_Exp. eauto. 
  - eapply T_Skip. 
  - eapply T_VDef; eauto.
  - eapply T_VAssign; eauto.
  - eapply T_SFAssign; eauto.
  - eapply T_If; eauto. 
  - eapply T_Seq; eauto.
  - eapply T_Loc; eauto. 
  - eapply T_Env; eauto.
  - eapply T_Const; eauto. 
  - eapply T_Rec; eauto. 
  - eapply T_Rec_TRd; eauto. 
  - eapply T_New; eauto. 
  - eapply T_SMCall; eauto. 
  - eapply T_ContAssign; eauto. 
  - eapply T_ObjNotation; eauto. 
  - eapply T_Cast; eauto. 
  - eapply T_Cast_TR; eauto. 
  - eapply T_Check_Cont_field; eauto. 
  - eapply T_Check_Cont_meth; eauto. 
  - eapply T_FldRd; eauto.
  - eapply T_FldRd_TR; eauto. 
 (* - eapply T_FldRd_getVal; eauto.   *)   
  - eapply T_ListExp_Empty; eauto.
  - eapply T_ListExp_Cons; eauto. 
  - eapply T_FDs_Empty; eauto. 
  - eapply T_FDs_Cons; eauto. 
  - intros. split; intros. 
    + admit.
    + split; intros; admit. 
  - eauto. 
  - eauto. 
Admitted.   

Lemma all_weakening : 
  (forall S Sigma Gamma ss s, type_reduction (typConx S Sigma Gamma) ss s ->
    (forall S' Sigma', Sig_Extends S' S -> ST_Extends Sigma' Sigma -> type_reduction (typConx S' Sigma' Gamma) ss s)) /\
  (forall S Sigma Gamma e t e', type_reduction_exps (typConx S Sigma Gamma) (e, t) e' ->
    (forall S' Sigma', Sig_Extends S' S -> ST_Extends Sigma' Sigma -> type_reduction_exps (typConx S' Sigma' Gamma) (e, t) e')) /\
  (forall S Sigma Gamma es ts es', type_reduction_exps_list (typConx S Sigma Gamma) (es, ts) es' ->
    (forall S' Sigma', Sig_Extends S' S -> ST_Extends Sigma' Sigma -> type_reduction_exps_list (typConx S' Sigma' Gamma) (es, ts) es')) /\
  (forall S Sigma Gamma fd fd', type_reduction_fieldDefns (typConx S Sigma Gamma) fd fd' ->
    (forall S' Sigma', Sig_Extends S' S -> ST_Extends Sigma' Sigma -> type_reduction_fieldDefns (typConx S' Sigma' Gamma) fd fd')) /\
  (forall S Sigma Gamma md md', type_reduction_methDefns (typConx S Sigma Gamma) md md' ->
    (forall S' Sigma', Sig_Extends S' S -> ST_Extends Sigma' Sigma -> type_reduction_methDefns (typConx S' Sigma' Gamma) md md')).
Proof.
  intros.  
  eapply typing_mutind; intros.
  - 
Admitted.

Lemma ST_Extends_refl : forall ST,
  ST_Extends ST ST.
Proof.
  intros.
  induction ST. eauto. eauto.
Qed.

Lemma Sig_Extends_refl : forall Sig,
  Sig_Extends Sig Sig.
Proof.
  intros.
  induction Sig. auto. auto. auto.
Qed.

Lemma e_not_error_s_not_error : forall e,
  e <> (beta_e_value beta_v_ERROR) <->
  (beta_s_exp e) <> (beta_s_exp (beta_e_value beta_v_ERROR)).
Proof.
  intros. split.  
  - unfold not. intros. inversion H0. rewrite H2 in H0. contradiction.
  - unfold not. intros. rewrite H0 in H. apply H. eauto.    
Qed.

Lemma noErrInListNoErr : forall e es,
  ~ (errorInListOfExps (e :: es)) ->
  e <> (beta_e_value beta_v_ERROR). 
Proof.
  unfold not. intros. subst.
  unfold errorInListOfExps in H. contradiction. 
Qed.

Lemma errInListCons : forall e es,
  errorInListOfExps (e :: es) ->
  e = (beta_e_value beta_v_ERROR) \/
  errorInListOfExps es.
Proof.
  intros.
  destruct e; try destruct b; unfold errorInListOfExps in H; eauto.
Qed.

Lemma noErrInListNoErrInList : forall e es,
  ~ (errorInListOfExps (e :: es)) ->
  ~ (errorInListOfExps es). 
Proof.
  dependent induction es.
  - intros. eauto.
  - intros. unfold not. intros HC.
    unfold not in H. eapply H. eapply errInListCons in HC. destruct HC.
    subst. destruct e; unfold errorInListOfExps; try destruct b; eauto.
    destruct e; destruct a; try destruct b; try destruct b0; try destruct b1; unfold errorInListOfExps; eauto.
Qed.

Lemma splitIntoTwo : forall A B (l : list (A * B)),
  exists left right,
    split l = (left, right).
Proof.
  intros. induction l.
  var_econ 2; simpl; eauto.
  destruct a. simpl. destruct IHl. destruct H. rewrite H. var_econ 2; simpl; eauto.
Qed.

Lemma fieldTypeRedListFieldTypeRed : forall lfnts lfes lfes' tC,
  type_reduction_fieldDefns tC (fDefn lfnts lfes) (beta_fDefn lfnts lfes') ->
  type_reduction_exps_list tC ((typedcontractAssignOverList_lfnts_exps lfnts lfes), (snd (split lfnts))) lfes'.
Proof.
  intros. remember (snd (split lfnts)) as theTypes.
  generalize dependent lfes. generalize dependent lfnts.
  dependent induction lfes'; intros.
  - inversion H; subst; eauto.
  - destruct (splitIntoTwo fieldName ty lfnts). destruct H0. rewrite H0 in HeqtheTypes.
    inversion H; subst.
    eapply IHlfes' in H8; eauto.
    simpl. destruct (splitIntoTwo fieldName ty lfnts0). destruct H1.
    simpl in H0. rewrite H1 in H0. inversion H0. subst.
    eapply T_ListExp_Cons; eauto.
    rewrite H1 in H8. simpl in H8. eauto.
Qed.

Lemma getMethNameTypeListFromType_from_subtyping : forall S t t' lmnts,
  lmnts = getMethNameTypeListFromType S t ->
  subtyping S t' t ->
  lmnts = getMethNameTypeListFromType S t'.
Proof. 
  intros. admit. 
Admitted. 

(*Lemma ifPrimSubtypePrimType : 
  forall S t t' pt,
    (t = (prim_dTyp pt)) /\ (subtyping S t t') ->
    (exists (pt' : primType), t' = (prim_dTyp pt')).
Proof.
  intros. inversion H. induction H1.
  - eauto.
  - destruct tau. 
    + eauto.
    + inversion H. 
Admitted.
*)    

(* ( forall TypCont s s', type_reduction TypCont s s' ->
        forall C, (value s') \/  (exists C' s'', reduction (C, s') (C', s'')))
  /\  ( forall TypCont et e', type_reduction_exps TypCont et e' ->
        (value (beta_s_exp e')) \/ forall C, (exists C' e'', reduction (C, (beta_s_exp e')) (C', (beta_s_exp e''))))
  /\  ( forall TypCont lett le', type_reduction_exps_list TypCont lett le' ->
        (expListAllValues le') \/ forall C, (exists C' le'', ListReduction (C, le') (C', le'')))
  (* ignore methods for now *)
  /\  ( forall TypCont lfds lfds', type_reduction_fieldDefns TypCont lfds lfds' ->
        (forall lfnts lfes', 
         (beta_fDefn lfnts lfes') = lfds' -> 
         ((beta_field_is_value lfds') \/ forall C, (exists C' lfes'', ListReduction (C, lfes') (C', lfes''))))). *)

Definition obj_well_typed (S : signature) (ST : store_typing) (Gamma : typeEnv) (obj : beta_runtimeObj) (ta : option ty) :=
  match ta with
  | None => False
  | Some t => forall f tf, (typeOfFieldInType f t S) = (Some tf) ->
                           exists e' e tf', (getFieldExpFromRunObj obj f) = (Some e) /\
                                             type_reduction_exps (typConx S ST Gamma) (e', tf') e /\ 
                                             subtyping S tf' tf
  end.

Definition heap_store_well_typed (S : signature) (ST : store_typing) (Gamma : typeEnv) (H : beta_heap) :=
  List.length ST = List.length H /\
   (forall l,
      l < List.length H ->
      (forall obj,
        (findObjInHeap H (thisLoc l)) = obj ->
        (obj_well_typed S ST Gamma obj (getLocTypeFromStoreTyping ST (thisLoc l)))) /\ 
      (forall p1 pt1, 
        findHeapContsAtLoc H (thisLoc l) = bheap_cont_prim p1 pt1 ->
        exists t'', getLocTypeFromStoreTyping ST (thisLoc l) = Some t'' ->
        subtyping S (prim_dTyp pt1) t'')).

Lemma sig_extends_typeOfFieldInType: forall S S' t f tf,
  typeOfFieldInType f t S = (Some tf) ->
  Sig_Extends S' S ->
  typeOfFieldInType f t S' = (Some tf).
Proof. 
  intros. destruct t; eauto. 
  simpl in *.
  destruct (getClass S c) eqn: HGet. symmetry in HGet.
  eapply sig_extends_getClass in HGet; eauto. rewrite <- HGet. eauto.
  inversion H.
Qed.

Lemma sig_extends_HWT : forall S S' Sigma H Gamma,
  heap_store_well_typed S Sigma Gamma H ->
  Sig_Extends S' S ->
  heap_store_well_typed S' Sigma Gamma H.
Proof. Admitted. 
 (*  intros.
  unfold heap_store_well_typed in *. destruct H0 as [HLen HObjWT].
  split; eauto. intros. eapply HObjWT in H0. destruct H0 as [obj HFImpl].
  exists obj. intros. eapply HFImpl in H0. clear HObjWT HFImpl.
  unfold obj_well_typed in *.
  destruct (getLocTypeFromStoreTyping Sigma (thisLoc l)) eqn: HGet; try (inversion H0; fail).
  intros. 
  Admitted.
 *)
  

Lemma ST_extends_HWT : forall S Sigma' Sigma H Gamma,
  heap_store_well_typed S Sigma Gamma H ->
  ST_Extends Sigma' Sigma ->
  heap_store_well_typed S Sigma' Gamma H.
Proof.
  
Admitted.

Lemma valFromObjIsFieldExp : forall f0 l l0 b l1 v, 
  valFromFieldName (Field f0) (getFieldsFromRunObj (brobj_fDefn l l0 b l1)) = Some v ->
  getFieldExpFromRunObj (brobj_fDefn l l0 b l1) (Field f0) = Some (beta_e_value v).
Proof. 
  intros. Admitted. 

Lemma nth_err_app_len : forall {A} (l:list A) lt,
 (nth_error (l ++ lt::nil) (Datatypes.length l) ) = Some lt.
Proof.
  intros. induction l.
  - simpl. auto.
  - simpl. auto.
Qed.

Lemma ifInStoreTypingLessLengthST : forall Sigma n t0,
  getLocTypeFromStoreTyping Sigma (thisLoc n) = Some t0 ->
  n < Datatypes.length Sigma.
Proof. 
  intros. inversion H. 
  assert (nth_error Sigma n <> None) as HPls. unfold not. intros. rewrite H1 in H0. inversion H0.    
  apply nth_error_Some in HPls. eauto. 
Qed.   

Scheme reduction_reduction_ind := Minimality for reduction Sort Prop
with reduction_listred_ind := Minimality for ListReduction Sort Prop
with reduction_exp_ind := Minimality for reduction_exps Sort Prop.

Combined Scheme reduction_mutind from reduction_reduction_ind, reduction_listred_ind, reduction_exp_ind.

Theorem preservation : 
    (forall p p', reduction p p' ->
    (forall S Sigma ss s s' Sig H X L Sig' H' X' L', 
    ((bstate_state Sig H X L), s) = p -> ((bstate_state Sig' H' X' L'), s') = p' ->
    s' <> (beta_s_exp (beta_e_value beta_v_ERROR)) ->
    type_reduction (typConx S Sigma []) ss s ->
     heap_store_well_typed S Sigma [] H -> 
      ( exists ss' S' Sigma', 
                          ST_Extends Sigma' Sigma /\
                          Sig_Extends S' S /\
                          type_reduction (typConx S' Sigma' []) ss' s' /\
                          heap_store_well_typed S' Sigma' [] H)))    /\
      
    (forall p p', ListReduction p p' ->
    (forall S Sigma es es' Sig H X L Sig' H' X' L' ts ess, 
     ((bstate_state Sig H X L), es) = p -> ((bstate_state Sig' H' X' L'), es') = p' ->
     ListReduction ((bstate_state Sig H X L), es) ((bstate_state Sig' H' X' L'), es') ->
    ~ (errorInListOfExps es') ->
    (*store_well_typed Delta H -> env_ok Gamma ->*)
    type_reduction_exps_list (typConx S Sigma []) (ess, ts) es ->
     heap_store_well_typed S Sigma [] H -> 
      (exists ess' S' Sigma' , 
                          ST_Extends Sigma' Sigma /\
                          Sig_Extends S' S /\
                          type_reduction_exps_list (typConx S' Sigma' []) (ess', ts) es' /\
                          heap_store_well_typed S' Sigma' [] H))) /\
                          
                          
    (forall p p', reduction_exps p p' ->
    (forall S Sigma t ee e e' Sig H X L Sig' H' X' L', 
    ((bstate_state Sig H X L), e) = p -> ((bstate_state Sig' H' X' L'), e') = p' ->
    e' <> (beta_e_value beta_v_ERROR) ->
    type_reduction_exps (typConx S Sigma []) (ee, t) e ->
    (*store_well_typed Delta H -> env_ok Gamma ->*)
     heap_store_well_typed S Sigma [] H -> 
      ( exists ee' t' S' Sigma', 
                          ST_Extends Sigma' Sigma /\
                          Sig_Extends S' S /\
                          subtyping S' t' t /\
                          type_reduction_exps (typConx S' Sigma' []) (ee', t') e' /\
                          heap_store_well_typed S' Sigma' [] H))).
Proof.
  apply reduction_mutind; intros.
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5; subst;
    econstructor; exists S, Sigma; eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H0. inversion H1. subst. clear H0 H1. inversion H3. subst. 
    econstructor. exists S, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl.
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst.
    edestruct H0; eauto. edestruct H3 as [S' [Sigma' HDo]].
    destruct HDo as [HST [HSig [HReduc HWT]]]. 
    econstructor. exists S', Sigma'. split; eauto. split; eauto. split.
    eapply T_Seq; eauto. eapply all_weakening; eauto. eauto. 
  * inversion H3. inversion H4. contradiction.
  * inversion H0. inversion H1. contradiction. 
  * inversion H3. inversion H4. subst. inversion H6. subst. clear H3 H4. 
    edestruct H0; eauto. 
    edestruct H3 as [tt' [S' [Sigma' HWow]]]. clear H3.
    destruct HWow. destruct H4. destruct H7. destruct H8. destruct H10. 
    exists (s_varAssign x tau x0). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_VDef. eauto. eauto. eapply sig_extends_subtyping with (S':=S') in H17; eauto. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. clear H6. 
    var_econ 1. exists S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. eauto.  
  * inversion H1. inversion H2. subst. contradiction.  
  * inversion H2. inversion H3. subst. contradiction.  
  * inversion H4. inversion H5. subst. clear H4 H5. inversion H7. subst. clear H7. 
    var_econ 1. exists S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. eauto. 
  * inversion H2. inversion H3. subst. contradiction.  
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H1. inversion H2. subst. contradiction. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst.
    edestruct H0; eauto. 
    edestruct H3 as [tt' [S' [Sigma' HWow]]]. clear H3.
    destruct HWow. destruct H4. destruct H8. destruct H9.
    var_econ 3; split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_VAssign. eapply all_weakening; eauto.
    eapply all_weakening; eauto using ST_Extends_refl, Sig_Extends_refl.
    eapply sig_extends_subtyping. eapply S_Transi. eauto. eapply sig_extends_subtyping; eauto. 
    eauto using Sig_Extends_refl.  
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H4. inversion H5. subst. clear H4 H5. inversion H7. subst. 
    econstructor. exists S0, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    econstructor. exists S0, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H0. inversion H1. subst. contradiction. 
  * inversion H1. inversion H2. subst. contradiction. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H3. inversion H4. subst. inversion H6. subst. clear H3 H4. 
    edestruct H0; eauto. 
    edestruct H3 as [tt' [S' [Sigma' HWow]]]. clear H3.
    destruct HWow. destruct H4. destruct H8. destruct H9.
    exists (s_fieldAssign x f e3). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_SFAssign. eauto. eapply all_weakening; eauto. 
    eapply sig_extends_subtyping with (S':=S') in H17. eapply S_Transi; eauto. eauto.    
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H2. inversion H3. subst. contradiction.  
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst.
    edestruct H0; eauto. 
    edestruct H3 as [tt' [S' [Sigma' HWow]]]. clear H3.
    destruct HWow. destruct H4. destruct H8. destruct H9. 
    exists (s_fieldAssign e1 f x). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail). 
    eapply T_SFAssign. 
    eapply all_weakening. eauto. eauto. eauto. eauto. admit. 
  * inversion H2. inversion H3. subst. clear H2 H3. contradiction.   
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst. 
    econstructor. exists S0, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst.
    econstructor. exists S0, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H1. inversion H2. subst. contradiction. 
  * inversion H3. inversion H4. subst. inversion H6.
  * inversion H3. inversion H4. subst. inversion H6. 
  * inversion H3. inversion H4. subst. inversion H6. 
  * inversion H1. inversion H2. subst. inversion H4. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst.
    econstructor. exists S, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl.  
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst. 
    econstructor. exists S, Sigma. eauto using Sig_Extends_refl, ST_Extends_refl. 
  * inversion H1. inversion H2. subst. contradiction. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    edestruct H0; eauto. 
    edestruct H3 as [tt' [S' [Sigma' HWow]]]. clear H3.
    destruct HWow. destruct H4. destruct H8. destruct H9. 
    exists (s_ifElse x s0 s3). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_If; eapply all_weakening; eauto using ST_Extends_refl, Sig_Extends_refl.   
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst. 
    edestruct H0; eauto. eapply e_not_error_s_not_error. eauto.  
    edestruct H2 as [tt' [S' [Sigma' HWow]]]. clear H2.
    destruct HWow. destruct H3. destruct H7. destruct H9. 
    exists (s_exp x). exists S', Sigma'. split; eauto. 
  (** inversion H7. inversion H8. subst. clear H7 H8. inversion H10. subst. clear H10. 
    var_econ 3. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. eauto. 
    admit. (* this is probably gonna need subst_preserves_typing *)*)
(* preservation for exps_list now *)
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H6. subst. 
    edestruct H0; eauto using noErrInListNoErr.
    edestruct H2 as [tt' [S' [Sigma' HWow]]]. clear H2.
    destruct HWow. destruct H3. destruct H8. destruct H9.
    exists (x :: eL). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_ListExp_Cons. 
    + eapply sig_extends_subtyping. eapply S_Transi. eauto. eapply sig_extends_subtyping. 
      eauto. eauto. eauto using Sig_Extends_refl.    
    + eauto.  
    + eapply all_weakening in H15; eauto. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H6. subst. 
    edestruct H0; eauto using noErrInListNoErrInList.
    edestruct H2 as [S' [Sigma' HWow]]. destruct HWow. destruct H8. destruct H9.  
    exists (e0 :: x). exists S', Sigma'. split; eauto. split; eauto. split; try (eauto; fail).
    eapply T_ListExp_Cons. 
    + eapply sig_extends_subtyping. eauto. eauto. 
    + eapply all_weakening; eauto. 
    + eauto. 
(* preservation for exps *)
  * inversion H0. inversion H1. subst. clear H0 H1. inversion H3; subst.
    + exists (e_theDefns Ms (fDefn lfnts (typedcontractAssignOverList_lfnts_exps lfnts lfes))).
      exists (getStructTypeFromETheDefns Ms (fDefn lfnts (typedcontractAssignOverList_lfnts_exps lfnts lfes))).
      var_econ 2. split; eauto using ST_Extends_refl. split; eauto using Sig_Extends_refl. split; try (eauto; fail).
      split; eauto using S_Refl. eapply T_Rec_TRd. eapply fieldTypeRedListFieldTypeRed. eauto.   
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    edestruct H0; eauto. 
    edestruct H3 as [S' [Sigma' HWow]]. clear H3. 
    destruct HWow. destruct H4. destruct H9. 
    var_econ 1. exists (getStructTypeFromETheDefns Ms (fDefn fnts x)). var_econ 2.
    split. eauto. split. eauto. split. eauto. split; try (eauto; fail). 
  * inversion H3. inversion H4. subst. contradiction. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst.
    var_econ 1. exists (getStructTypeFromETheDefns Ms (fDefn lfnts lfes)). exists S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl.
    split. eauto. eauto.     
  * inversion H5. inversion H6. subst. clear H5 H6. inversion H8. subst. 
    var_econ 1. exists ((getStructTypeFromETheDefns Ms (fDefn lfnts lfes))). exists S0, (Sigma ++ [(getStructTypeFromETheDefns Ms (fDefn lfnts lfes))]). split. 
    eauto using ST_extends_app. split. eauto using Sig_Extends_refl. split. eauto. split; try (eauto using ST_extends_HWT; fail). 
    eapply T_Loc. simpl.
    assert (stLength H = stLength Sigma). 
    + inversion H9. unfold stLength. eauto.
    + rewrite H1. unfold stLength. apply nth_err_app_len.
    + eapply ST_extends_HWT. eauto. eapply ST_extends_app.  
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst.
    edestruct H0; try (eauto using noErrInListNoErrInList; fail). 
    edestruct H3. edestruct H4. edestruct H9. edestruct H11. edestruct H13.  
    var_econ 1. exists (getStructTypeFromETheDefns Ms (fDefn fnts x)). var_econ 2. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. split. eauto using S_Refl. split; try (eauto; fail). 
  * inversion H3. inversion H4. clear H3 H4. subst. contradiction. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. 
  * inversion H3. inversion H4. subst. clear H3 H4. contradiction.
  * inversion H4. inversion H5. subst. clear H4 H5. inversion H7. 
  * inversion H4. inversion H5. subst. contradiction. 
  * inversion H5. inversion H6. subst. clear H5 H6. inversion H8. subst. 
    inversion H2; subst. assert (H9':=H9). unfold heap_store_well_typed in H9. destruct l.
    assert (n < Datatypes.length Sigma). eapply ifInStoreTypingLessLengthST in H1. eauto. 
    destruct H9 as [HLen HSTWT]. rewrite HLen in H0. 
    destruct (findObjInHeap H (thisLoc n)) eqn: HObj.
    (* null *) inversion H3.
    (* not  *) eapply HSTWT in H0; eauto. clear HSTWT. destruct H0. 
    symmetry in HObj. unfold obj_well_typed in H0. rewrite H1 in H0. 
    eapply H0 in H12; eauto. destruct H12 as [e' [e [tf' [HGet [HType HSub]]]]]. 
    apply valFromObjIsFieldExp in H3. rewrite HObj in H3. rewrite H3 in HGet. inversion HGet; subst; clear HGet.
    var_econ 4; split; eauto using ST_Extends_refl. split; eauto using Sig_Extends_refl.
    inversion H1. 
  * inversion H5. inversion H6. subst. contradiction. 
  * inversion H6. inversion H7. subst. inversion H9. 
  * inversion H6. inversion H7. subst. inversion H9. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. inversion H3.  
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. inversion H3. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst.
    edestruct H0; eauto.  
    edestruct H3 as [t1 [S' [Sigma' HWow]]]. clear H3. 
    destruct HWow. destruct H4. destruct H8. destruct H9. 
    var_econ 1. exists t, S', Sigma'. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. split; try (eauto; fail). 
    split. eauto. 
    eapply T_SMCall; eauto. eapply all_weakening in H15; eauto. eapply sig_extends_typeListSubtyping in H16; eauto.
    admit.
    eauto.  
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H4. inversion H5. subst. clear H4 H5. inversion H7. subst. admit. 
  * inversion H4. inversion H5. subst. contradiction. 
  * inversion H6. inversion H7. subst. contradiction. 
  * inversion H7. inversion H8. subst. contradiction.
  * inversion H7. inversion H8. subst. clear H7 H8. inversion H10. subst. admit.   
  * inversion H0. inversion H1. subst. clear H0 H1. inversion H3. subst.
    exists (e_cast t (e_contAssign (t_CA e0 t0))). exists t0. exists S, Sigma. 
    split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. split. eauto. split; try (eauto; fail).
    (*eapply T_Cast_TR. eapply T_ContAssign. eauto. eauto. eauto. *)
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    edestruct H0; eauto. 
    edestruct H3 as [t' [S' [Sigma' Hwow]]].
    destruct Hwow. destruct H9. destruct H10. destruct H11.
    var_econ 2. exists S', Sigma'. split. eauto. split. eauto. split. eauto. split; try (eauto; fail). 
    eapply T_Cast_TR; eauto. eapply sig_extends_subtyping in H14; eauto. 
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H1. inversion H2. subst. clear H1 H2. inversion H4. subst. clear H4. 
    destruct ev; try (inversion H; fail).
    var_econ 2. exists S, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. eauto. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst. clear H5. 
    admit. 
    admit. 
  * inversion H1. inversion H2. subst. clear H1 H2. inversion H4. subst. clear H4. 
    var_econ 1. exists t'', S, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. 
    split. eapply sig_extends_subtyping in H12; eauto. eauto using Sig_Extends_refl. eauto. 
  * inversion H1. inversion H2. subst. contradiction. 
  * inversion H0. inversion H1. subst. contradiction. 
  * inversion H2. inversion H3. subst. clear H2 H3. inversion H5. subst.
    inversion H6. destruct loc1.  
    var_econ 1. exists t'', S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. 
    split. eauto. split; try (eauto; fail). eapply T_ContAssign. eauto. inversion H3. subst.
    edestruct H2 with (l:=n). eapply ifInStoreTypingLessLengthST in H8. rewrite <- H1. eauto.
    admit.
    inversion H0.
  * inversion H3. inversion H4. subst. contradiction. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    var_econ 1. exists t'', S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. 
    split. eauto. eauto. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    edestruct H0; eauto using e_not_error_s_not_error.
    edestruct H3 as [t'' [S' [Sigma' Hwow]]].
    destruct Hwow. destruct H9. destruct H10. destruct H11.
    exists (e_checkCont (t_CC x m)). exists t, S', Sigma'. split. eauto. split. eauto. split. eauto. split; try (eauto; fail).
    (*eapply T_Check_Cont_meth. eauto. *)
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. 
    + edestruct H0; eauto. edestruct H3 as [t'' [S' [Sigma' Hwow]]].
      destruct Hwow. destruct H8. destruct H9. destruct H11.
      inversion H12; subst.
      exists (e_checkCont (t_CC x f)). exists t, S', Sigma'. split. eauto. split. eauto. split. eauto. split; try (eauto; fail).
      eapply T_Check_Cont_field. eauto. 
      eapply T_FldRd_TR. eauto. admit.
    + edestruct H0; eauto. edestruct H15 as [t'' [S' [Sigma' Hwow]]].
      destruct Hwow. destruct H17. destruct H18. destruct H19. 
      exists (e_checkCont (t_CC x f)). exists t, S', Sigma'. split. eauto. split. eauto. split. eauto. split; try (eauto; fail).
      eapply T_Check_Cont_field. eauto. eapply T_FldRd_TR. eauto. admit. 
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H4. inversion H5. subst. contradiction. 
  * inversion H4. inversion H5. subst. contradiction. 
  * inversion H5. inversion H6. subst. contradiction. 
  * inversion H5. inversion H6. subst. contradiction. 
  * inversion H6. inversion H7. subst. clear H6 H7. inversion H9. subst. clear H9. 
    + var_econ 1. exists t, S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. 
      split. eauto. split; try (eauto; fail). inversion H12. subst.
      admit. 
    + var_econ 1. exists t, S0, Sigma. split. eauto using ST_Extends_refl. split. eauto using Sig_Extends_refl. 
      split. eauto. split; try (eauto; fail). admit.
  * inversion H5. inversion H6. subst. clear H5 H6. inversion H8. subst. clear H8. 
    destruct H9. destruct loc.
    + admit.
    + inversion H2. 
    var_econ 1. exists t. exists S0, Sigma. 
    split; eauto using ST_Extends_refl. split; eauto using Sig_Extends_refl. split; eauto using S_Refl. split; try (eauto; fail).
    eapply T_ContAssign. eauto. admit. admit.   
  * inversion H3. inversion H4. subst. clear H3 H4. inversion H6. subst. clear H6. 
    edestruct H0; eauto. edestruct H3 as [t' [S' [Sigma' Hwow]]]. 
    destruct Hwow. destruct H6. destruct H9. destruct H10.  
    exists (e_fieldAcc x (Field f0)). exists t. exists S', Sigma'. split. eauto. split. eauto. split. eauto. split; try (eauto; fail).
    eapply T_FldRd_TR. eauto. admit. 
  * inversion H2. inversion H3. subst. contradiction. 
  * inversion H2. inversion H3. subst. contradiction.
  * inversion H4. inversion H5. subst. contradiction.   
Admitted.  

Definition methDefnIsValue (md : beta_methDefn) : Prop :=
  True.
  
Parameter e_error_eq_dec : forall e: beta_exp, {e = (beta_e_value beta_v_ERROR)} + {e <> (beta_e_value beta_v_ERROR)}.
Parameter s_skip_eq_dec : forall s: beta_stat, {s = beta_s_skip} + {s <> beta_s_skip}.
Parameter e_C_redErr : forall (C : beta_state) (e : beta_exp), {exists C', C / e ~~> C' / (beta_e_value beta_v_ERROR)} + {~(exists C', C / e ~~> C' / (beta_e_value beta_v_ERROR))}.
Parameter s_C_redErr : forall (C : beta_state) (s : beta_stat), {exists C', C / s --> C' / (beta_s_exp (beta_e_value beta_v_ERROR))} + {~(exists C', C / s --> C' / (beta_s_exp (beta_e_value beta_v_ERROR)))}.
Parameter objVal_dec : forall (bo : beta_objRep), {objIsValue bo} + {~(objIsValue bo)}.

  
Lemma redExpMeansRedStat : forall e e' C C',
  C / e ~~> C' / e' ->
  C / (beta_s_exp e) --> C' / (beta_s_exp e').
Proof. 
  intros. eapply beta_E_e_steps. eauto. 
Qed. 

Lemma notAllValsMeansReduction : forall C les C' les',
  ~ (expListAllValues les) ->
  ListReduction (C, les) (C', les').
Proof. 
  admit.
Admitted.   
(*
Lemma redStatMeansRedExp : forall e e' C C',
  C / (beta_s_exp e) --> C' / (beta_s_exp e') ->
  C / e ~~> C' / e'.
Proof.
  intros. admit. 
Admitted. *)
  
  
Theorem progress : 
      ( forall TypCont s s', type_reduction TypCont s s' ->
        forall C, ((value s') \/ (s' = beta_s_skip)) \/  (exists C' s'', reduction (C, s') (C', s'')))
  /\  ( forall TypCont et e', type_reduction_exps TypCont et e' ->
        (value (beta_s_exp e')) \/ forall C, (exists C' e'', reduction_exps (C, e') (C', e'')))
  /\  ( forall TypCont lett le', type_reduction_exps_list TypCont lett le' ->
        (expListAllValues le') \/ forall C, (exists C' le'', ListReduction (C, le') (C', le'')))
  /\  ( forall TypCont lfds lfds', type_reduction_fieldDefns TypCont lfds lfds' ->
        (forall lfnts lfes', 
         (beta_fDefn lfnts lfes') = lfds' -> 
         ((beta_field_is_value lfds') \/ forall C, (exists C' lfes'', ListReduction (C, lfes') (C', lfes'')))))
  /\ ( forall TypCont lmds lmds', type_reduction_methDefns TypCont lmds lmds' -> 
        (methDefnIsValue lmds')).
Proof.
  eapply typing_mutind; eauto.
  - intros. inversion H0. 
    + left. left. eauto. 
    + right. edestruct H1. edestruct H2. exists x, (beta_s_exp x0). eapply redExpMeansRedStat. eauto.
    Unshelve.
  - intros. right. inversion H0.
    + destruct C. inversion H3.  
      * (* is primval *)
        destruct t'. 
        destruct (prim_ty_eq_dec pt p0); destruct p; destruct pt; 
        try (var_econ 2; eapply beta_E_VarDef_prim_badType; eauto; fail); 
        try (var_econ 2; eapply beta_E_VarDef_prim_ok; eauto; fail).
        
        var_econ 2. eapply beta_E_VarDef_prim_objType. 
        eapply ifObjSubtypeObjType with (oT:=(mf_dTyp m f)) (S:=S); eauto.
        
        var_econ 2. eapply beta_E_VarDef_prim_objType. 
        eapply ifObjSubtypeObjType with (oT:=Any) (S:=S); eauto.
        
        var_econ 2. eapply beta_E_VarDef_prim_objType. 
        eapply ifObjSubtypeObjType with (oT:=(inter_dTyp i)) (S:=S); eauto.
        
        var_econ 2. eapply beta_E_VarDef_prim_objType. 
        eapply ifObjSubtypeObjType with (oT:=(class_dTyp c)) (S:=S); eauto.
      * (* is loc *)
        remember (findHeapContsAtLoc b l) as tg.
        inversion Heqtg.
        destruct l. var_econ 2. eapply beta_E_VarDef_loc; eauto. eauto.
        (*(* nulrobj *)
        var_econ 2; eapply beta_E_VarDef_loc; eauto.  
        eapply beta_E_Val. subst. eauto.
        (* not-nul obj defn *) 
        var_econ 2. eapply beta_E_VarDef_canRed.
        rewrite Heqtg in H3.
        eapply beta_E_Val_runObj. eauto. 
        (* prim value *)
        var_econ 2. eapply beta_E_VarDef_canRed.
        rewrite Heqtg in H3. 
        eapply beta_E_Val_primLoc. eauto.    *)
    + edestruct (e_C_redErr C e').
      * destruct e0. exists x0. var_econ 1. eapply beta_E_VarDef_canRed_ERR. eapply beta_E_e_steps. eauto. 
      * edestruct H3. edestruct H4. var_econ 2. eapply beta_E_VarDef_canRed. eauto. 
        unfold not in n. unfold not. intros. subst. apply n. exists x0. eauto.
  Unshelve. 
  - intros. right. inversion H0.
    + destruct C. inversion H2; try (inversion H4; fail).
    + destruct C. inversion H2.
      * inversion H5.
        (* is primval *)
        destruct t'; 
        
        remember ((beq_loc (locFromVarInStore b1 x) nulLoc)) as tg; destruct tg;
        try ( var_econ 2; eapply beta_E_VarUpd_BadLoc; eauto; fail);
        try ( var_econ 2; eapply beta_E_VarUpd_prim; eauto; rewrite <- Heqtg; unfold not; intros HContra; inversion HContra; fail).
        
        (* is loc *)
        remember ((beq_loc (locFromVarInStore b1 x) nulLoc)) as tg. destruct tg.
        destruct l. 
        var_econ 2. eapply beta_E_VarUpd_BadLoc; eauto. 
        var_econ 2. eapply beta_E_VarUpd_loc_nil; eauto.
        
        destruct l. 
        var_econ 2. eapply beta_E_VarUpd_loc; eauto. unfold not. intros. rewrite H6 in Heqtg. inversion Heqtg.
        var_econ 2. eapply beta_E_VarUpd_loc_nil; eauto.     
        
        (*remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        destruct l. var_econ 2. eapply beta_E_VarUpd_loc. 
        
         eauto. eauto.
        var_econ 2. eapply beta_E_VarUpd_canRed. eapply beta_E_Val_badLoc. eauto.
        var_econ 2. eapply beta_E_VarUpd_canRed. eapply beta_E_Val_runObj. eauto.
        var_econ 2. eapply beta_E_VarUpd_canRed. eapply beta_E_Val_primLoc. eauto.*)
      * edestruct (e_C_redErr (bstate_state s b b0 b1) e').
        
        edestruct H5. edestruct H6.
        destruct e0. exists x2. var_econ 1. eapply beta_E_VarUpd_canRed_ERR. eapply beta_E_e_steps. eauto.
        
        edestruct H5. edestruct H6. var_econ 2. eapply beta_E_VarUpd_canRed. eauto. 
        unfold not in n. unfold not. intros. subst. apply n. exists x0. eauto.  
  - intros. right. inversion H0.
    + inversion H2.
      * destruct e1'; try (inversion H4; fail).
        destruct b.
        destruct e2'; try (inversion H5; fail).
        destruct b; destruct C. remember (findHeapContsAtLoc b l0) as tg. destruct tg.
        var_econ 2. eapply beta_E_SfldUpd_loc. eauto. 
        var_econ 2. eapply beta_E_SfldUpd_loc. eauto. 
        var_econ 2. eapply beta_E_SfldUpd_prim. eauto.
        var_econ 2. eapply beta_E_SfldUpd_ERR. 
        var_econ 2. eapply beta_E_SfldUpd_firstBadVal. eauto. 
                    unfold not. intros. inversion H6.
        var_econ 2. eapply beta_E_SfldUpd_firstBadVal. eauto. 
                    unfold not. intros. inversion H6.
      * destruct e1'; try (inversion H4; fail).
        destruct b; destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg.
        
        remember (bstate_state s b b0 b1) as C.
        edestruct (e_C_redErr C e2').
        edestruct e. exists x. var_econ 1. eapply beta_E_SfldUpd_canRed_ERR. eapply beta_E_e_steps. eauto. 
        edestruct H5 with (C:=C). edestruct H6. 
        var_econ 2. eapply beta_E_SfldUpd_canRed. eauto. unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.  
        
        remember (bstate_state s b b0 b1) as C.
        edestruct (e_C_redErr C e2').
        edestruct e. exists x. var_econ 1. eapply beta_E_SfldUpd_canRed_ERR. eapply beta_E_e_steps. eauto. 
        edestruct H5 with (C:=C). edestruct H6. 
        var_econ 2. eapply beta_E_SfldUpd_canRed. eauto. unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.  
        
        var_econ 2. eapply beta_E_SfldUpd_firstBadVal. eauto. unfold not. intros HContra. inversion HContra. 
        var_econ 2. eapply beta_E_SfldUpd_firstBadVal. eauto. unfold not. intros HContra. inversion HContra.
    + edestruct (e_C_redErr C e1').
      destruct e. exists x. var_econ 1. eapply beta_E_SfldUpd_firstRed_ERR. eapply beta_E_e_steps. eauto. 
      edestruct H4. edestruct H5. var_econ 2. eapply beta_E_SfldUpd_firstRed. eauto. 
      unfold not in n. unfold not. intros. subst. apply n. exists x. eauto. 
  - intros. right. inversion H0.
    + inversion H5.
      * destruct (prim_ty_eq_dec pt prim_Bool).
        destruct p.
        destruct b.
  (* true *) var_econ 2. eapply beta_E_If_boolVal_true. subst. eauto. eauto.
  (* false *) var_econ 2. eapply beta_E_If_boolVal_false. subst. eauto. eauto. 
  (* broken *) var_econ 2. eapply beta_E_If_badVal. unfold not. intros. inversion H6. 
  (* broken *) var_econ 2. eapply beta_E_If_badVal. unfold not. intros. inversion H6. 
  (* broken *) var_econ 2. eapply beta_E_If_badVal. unfold not. intros. inversion H6. 
  (* broken *) var_econ 2. eapply beta_E_If_badVal. unfold not. intros. inversion H6.
   (* the other section of destruct p *)      
        var_econ 2. eapply beta_E_If_badVal.
        unfold not. intros. inversion H6. contradiction.
      * var_econ 2. eapply beta_E_If_badVal. unfold not. intros. inversion H6.
    + edestruct (e_C_redErr C e').
      destruct e0. exists x. var_econ 1. eapply beta_E_If_canRed_err. eapply beta_E_e_steps. eauto. 
      edestruct H5. edestruct H6. var_econ 2. eapply beta_E_If_canRed. eauto. 
      unfold not in n. unfold not. intros. subst. apply n. exists x. eauto. 
  - intros. right. edestruct H0.
    + destruct H3. destruct H3. 
      * var_econ 2. eapply beta_E_Seq_canRed. eapply beta_E_Val; eauto.      
        unfold not. intros. inversion H3.  
        unfold not. intros. inversion H3.
      * var_econ 2. eapply beta_E_Seq_canRed. eapply beta_E_Val; eauto. 
        unfold not. intros. inversion H3. 
        unfold not. intros. inversion H3.
      * var_econ 2. subst. eapply beta_E_Seq_Skip.  
    + edestruct (s_C_redErr C s1').
      * edestruct H3. edestruct H4.
        edestruct e. exists x1. var_econ 1. eapply beta_E_Seq_canRed_ERR. eauto. eauto.
      * edestruct H3. edestruct H4. 
        var_econ 2. eapply beta_E_Seq_canRed. eauto. unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.
  - (* after stats *)
    intros. right.
    intros. 
    destruct C. remember (locFromVarInStore b1 x) as tg. inversion Heqtg.
    destruct (locFromVarInStore b1 x) in H0. 
    + remember (findHeapContsAtLoc b tg) as tg1. 
      inversion Heqtg1. destruct (findHeapContsAtLoc b tg) in H1.
        econstructor; econstructor. eapply beta_E_Var_obj. 
        rewrite H0 in Heqtg. eauto.
        rewrite H1 in Heqtg1. rewrite H0 in Heqtg. subst. eauto.
        econstructor; econstructor. eapply beta_E_Var_prim.
        rewrite H0 in Heqtg. eauto.
        rewrite H1 in Heqtg1. rewrite H0 in Heqtg. subst. eauto.
    + econstructor; econstructor. eapply beta_E_Var_noLoc. subst. eauto.
  - intros. destruct H.
    + right. intros. destruct C. edestruct H0; eauto.
    + right. intros. destruct C. destruct H0.
      var_econ 2. eapply beta_E_ObjLit_isVal; eauto.
      edestruct H0. edestruct H3.
      destruct (errorInListOfExps_eq_dec x0).
      var_econ 2. eapply beta_E_ObjLit_canRed_ERR; eauto. 
      var_econ 2. eapply beta_E_ObjLit_canReduce; eauto.     
  - intros. destruct H.
    + right. intros. destruct C. induction H0.
      * var_econ 2. eapply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New; eauto.
      * var_econ 2. eapply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New; eauto.
    + right. intros. destruct C. induction H0.
      remember (CA_list (be :: beL)) as Pls. induction Pls. 
      * var_econ 2. eapply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps.
        eapply beta_E_New; eauto.
      * remember (expListAllValues (a :: l)) as ValsExpList. 
        remember (~ errorInListOfExps (a :: l)) as NoErrsExpList. 
        destruct (expListValues_eq_dec (a :: l)).
        destruct (errorInListOfExps_eq_dec (a :: l)).
        (* all values, but error in values *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New_wrongParamErr; eauto.
        (* no issues *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New; eauto.
        (* not all values *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New_NotAllVals; eauto.
      * remember (expListAllValues (CA_list (be :: beL) ltprms)) as ValsExpList. 
        remember (~ errorInListOfExps (CA_list (be :: beL) ltprms)) as NoErrsExpList. 
        destruct (expListValues_eq_dec (CA_list (be :: beL) ltprms)).
        destruct (errorInListOfExps_eq_dec (CA_list (be :: beL) ltprms)).
        (* all values, but error in values *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New_wrongParamErr; eauto.
        (* no issues *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New; eauto.
        (* not all values *)
        var_econ 2. apply beta_E_contractAssign_canReduce. unfold CA_list. eapply beta_E_e_steps. 
        eapply beta_E_New_NotAllVals; eauto.
  - intros. right. intros. 
    destruct H0. 
    + inversion H0. 
      * var_econ 2. eapply beta_E_SMCall_OutBad; eauto. unfold not. intros HF. inversion HF. 
      * remember (expListAllValues (CA_list lexps' lts)) as ValsExpList. 
        remember (~ errorInListOfExps (CA_list lexps' lts)) as NoErrsExpList. 
        destruct (expListValues_eq_dec (CA_list lexps' lts)).
        destruct (errorInListOfExps_eq_dec (CA_list lexps' lts)).
        var_econ 2. eapply beta_E_SMCall_InErrList; eauto. 
        destruct C. remember (findHeapContsAtLoc b l) as HeapContsObj. destruct HeapContsObj.
        remember (getMethDefnFromRunObj b2 mN) as IsMethod. destruct IsMethod.
        destruct b3. 
        var_econ 2. eapply beta_E_SMCall_M_S; eauto.  
        var_econ 2. eapply beta_E_SMCall_NoM; eauto.
        var_econ 2. eapply beta_E_SMCall_locPrim; eauto. 
        var_econ 2. eapply beta_E_SMCall_InRed; eauto. apply notAllValsMeansReduction. eauto.  
    + edestruct H0. edestruct H5.
      destruct (e_error_eq_dec x0).
      var_econ 2. eapply beta_E_SMCall_OutRed_ERR; eauto. subst. eauto. 
      var_econ 2. eapply beta_E_SMCall_OutRed; eauto.   
  - intros. right. intros. 
    destruct H0. 
    + inversion H0. 
      * destruct t. destruct (prim_ty_eq_dec pt p0). 
        var_econ 2. eapply beta_E_contractAssign_onPrimVal. rewrite e0. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onPrimVal_badType. unfold not in n. unfold not. intros. apply n. inversion H2. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onPrimVal_badType. unfold not. intros. congruence. 
        var_econ 2. eapply beta_E_contractAssign_onPrimVal_badType. unfold not. intros. congruence.
        var_econ 2. eapply beta_E_contractAssign_onPrimVal_badType. unfold not. intros. congruence.
        var_econ 2. eapply beta_E_contractAssign_onPrimVal_badType. unfold not. intros. congruence.
      * destruct t. destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_badLoc. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_runObj_badType. eauto. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_primVal. eauto. 
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply beta_E_contractAssign_onLoc_badLoc. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_primVal. eauto.
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply beta_E_contractAssign_onLoc_badLoc. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_primVal. eauto.
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply beta_E_contractAssign_onLoc_badLoc. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_primVal. eauto.
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply beta_E_contractAssign_onLoc_badLoc. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply beta_E_contractAssign_onLoc_primVal. eauto.
    + edestruct H0. edestruct H2. var_econ 2. eapply beta_E_contractAssign_canReduce. eauto.
  - intros. right. intros. destruct H0.
    + destruct C. (*destruct (objVal_dec (bobj_fDefn l [Ms'] (beta_fDefn lfnts lfes') cs')). *)
      var_econ 2. eapply beta_E_Obj_redToRunObj; eauto.
    + edestruct H0. edestruct H1. 
      destruct (errorInListOfExps_eq_dec x0).
      destruct C. var_econ 2. eapply beta_E_Obj_canRed_ERR; eauto. 
      destruct C. var_econ 2. eapply beta_E_Obj_canRed; eauto. 
  - intros. right. intros. destruct H0. 
    + var_econ 2. eapply beta_E_cast_isVal. eauto. 
    + edestruct (e_C_redErr C e').
      * edestruct e0. exists x. var_econ 1. eapply beta_E_cast_canRed_ERR. eauto.
      * edestruct H0. edestruct H2. 
        var_econ 2. eapply beta_E_cast_canRed. eauto. unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.
  - intros. right. intros. destruct H0. 
    + inversion H0. 
      * var_econ 2. eapply beta_E_checkContract_NotLoc_field. eauto. unfold not. intros Hc. congruence.
      * destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. 
        destruct b2. 
        var_econ 2. eapply beta_E_checkContract_Loc_NoField; eauto.
        remember (getFieldExpFromRunObj (brobj_fDefn l0 l1 b2 l2) (Field f)) as bfr. destruct bfr.
        var_econ 2. eapply beta_E_checkContract_Obj_Field; eauto.
        var_econ 2. eapply beta_E_checkContract_Loc_NoField; eauto. 
        var_econ 2. rewrite H4. eapply beta_E_checkContract_Loc_prim_field; eauto.
    + edestruct (e_C_redErr C e').
      * edestruct e0. exists x. var_econ 1. eapply beta_E_checkContract_canReduce_field_ERR. eauto. 
      * edestruct H0. edestruct H3. 
        var_econ 2. eapply beta_E_checkContract_canReduce_field. eauto.
        unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.  
  - intros. right. intros. destruct H0. 
    + inversion H0. 
      * var_econ 2. eapply beta_E_checkContract_NotLoc_meth. eauto. unfold not. intros Hc. congruence.
      * destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. 
        destruct b2. 
        var_econ 2. eapply beta_E_checkContract_Obj_NoMeth; eauto. 
        var_econ 2. eapply beta_E_checkContract_Obj_Meth; eauto. (* red rule for cc on methods *)
        var_econ 2. eapply beta_E_checkContract_Loc_prim_meth; eauto. 
    + edestruct (e_C_redErr C e').
      * edestruct e0. exists x. var_econ 1. eapply beta_E_checkContract_canReduce_meth_ERR. eauto.  
      * edestruct H0. edestruct H1.
        var_econ 2. eapply beta_E_checkContract_canReduce_meth. eauto.
        unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.  
  - intros. right. intros. destruct H0.
    + inversion H0. 
      * var_econ 2. eapply beta_E_checkContract_NotLoc_field. eauto. unfold not. intros Hc. congruence.
      * destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. 
        destruct b2. 
        var_econ 2. eapply beta_E_checkContract_Loc_NoField; eauto.
        remember (getFieldExpFromRunObj (brobj_fDefn l0 l1 b2 l2) (Field f)) as bfr. destruct bfr.
        var_econ 2. eapply beta_E_checkContract_Obj_Field; eauto.
        var_econ 2. eapply beta_E_checkContract_Loc_NoField; eauto. 
        var_econ 2. eapply beta_E_checkContract_Loc_prim_field; eauto.
    + edestruct (e_C_redErr C e').
      * edestruct e0. exists x. var_econ 1. eapply beta_E_checkContract_canReduce_field_ERR. eauto. 
      * edestruct H0. edestruct H2. 
        var_econ 2. eapply beta_E_checkContract_canReduce_field. eauto.
        unfold not in n. unfold not. intros. subst. apply n. exists x. eauto. 
  - intros. right. intros. destruct H0. 
    + inversion H0. 
      * var_econ 2. eapply beta_E_fieldAcc_notLoc; eauto. unfold not. intros. congruence. 
      * destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg.  
        remember (valFromFieldName (Field f) (getFieldsFromRunObj (findObjInHeap b l))) as tfg. destruct tfg.
        var_econ 2. eapply beta_E_Sproj_vThere; eauto.
        var_econ 2. eapply beta_E_Sproj_vNotThere; eauto. 
        var_econ 2. eapply beta_E_fieldAcc_LocPrim; eauto. 
    + edestruct (e_C_redErr C e').
      * edestruct e0. exists x. var_econ 1. eapply beta_E_fieldAcc_canRed_ERR. eauto. 
      * edestruct H0. edestruct H1. destruct H2.  
        var_econ 2. eapply beta_E_fieldAcc_canRed. eauto. 
        unfold not in n. unfold not. intros. subst. apply n. exists x. eauto.     
  - intros S Sigma Gamma e eL t tL be beL t' Hsub. intros. destruct H. 
    + induction H2.
      * left. eauto. 
      * right. intros. destruct H2 with C. edestruct H3.
        var_econ 2. eapply LR_cons. eauto.
    + right. intros. induction H2.
      * inversion H0.
        inversion H3.
        edestruct H3. edestruct H4. 
        var_econ 2. eapply LR_one. eapply H5.
      * inversion H0. 
        inversion H3.
        edestruct H3. edestruct H4.
        var_econ 2. eapply LR_one. eapply H5.
    + induction H2.
      * left. eauto.
      * right. intros. destruct H with C. edestruct H2.
        var_econ 2. eapply LR_cons. eauto.
    + right. intros. induction H2.
      * inversion H0.
        inversion H4.
        edestruct H4. edestruct H5.
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0.
        inversion H4.
        edestruct H4. edestruct H5.
        var_econ 2. eapply LR_one. eapply H6.
    + right. intros. induction H2.
      * inversion H0.
        inversion H3.
        edestruct H3. edestruct H4.
        var_econ 2. eapply LR_one. eapply H5.
      * inversion H0.
        inversion H3.
        edestruct H3. edestruct H4.
        var_econ 2. eapply LR_one. eapply H5.
    + right. intros. induction H2.
      * inversion H0.
        inversion H7.
        edestruct H7. edestruct H8.
        var_econ 2. eapply LR_one. eapply H9.
      * inversion H0.
        inversion H7.
        edestruct H7. edestruct H8.
        var_econ 2. eapply LR_one. eapply H9.
    + right. intros. induction H2.
      * inversion H0.
        inversion H6.
        edestruct H6. edestruct H7.
        var_econ 2. eapply LR_one. eapply H8.
      * inversion H0.
        inversion H6.
        edestruct H6. edestruct H7.
        var_econ 2. eapply LR_one. eapply H8.
    + right. intros. induction H2.
      * inversion H0.
        inversion H4.
        edestruct H4. edestruct H5.
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0.
        inversion H4.
        edestruct H4. edestruct H5.
        var_econ 2. eapply LR_one. eapply H6. 
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H3. 
        edestruct H3. edestruct H4. 
        var_econ 2. eapply LR_one. eapply H5.
      * inversion H0. 
        inversion H3. 
        edestruct H3. edestruct H4. 
        var_econ 2. eapply LR_one. eapply H5.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H3. 
        edestruct H3. edestruct H4. 
        var_econ 2. eapply LR_one. eapply H5.
      * inversion H0. 
        inversion H3. 
        edestruct H3. edestruct H4. 
        var_econ 2. eapply LR_one. eapply H5.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
    + right. intros. induction H2. 
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
      * inversion H0. 
        inversion H4. 
        edestruct H4. edestruct H5. 
        var_econ 2. eapply LR_one. eapply H6.
  - intros. destruct H. 
    + right. intros. edestruct H3; eauto.  
      * inversion H4. inversion H0.
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_badLoc. eauto.
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_primVal. eauto.
        edestruct H6. edestruct H9.  
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.  
      * inversion H4. inversion H0. 
        destruct C. remember (findHeapContsAtLoc b l) as tg. destruct tg. destruct b2.
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_badLoc. eauto.
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_runObj. eauto. eauto. 
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_onLoc_primVal. eauto.
        edestruct H6. edestruct H9.  
        var_econ 2. eapply LR_one. eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.   
    + right. intros. edestruct H3; eauto.  
      * inversion H4. inversion H0.
        inversion H6.
        edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
        eapply beta_E_contractAssign_canReduce. eauto.
      * inversion H4. inversion H0.
        inversion H6.
        edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
        eapply beta_E_contractAssign_canReduce. eauto.
    + right. intros. edestruct H3; eauto.  
      * inversion H4. inversion H0.
        (* is primval *)
        destruct t1. destruct (prim_ty_eq_dec pt p).
   (* == *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal. subst. auto.
   (* <> *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
            intros HCont. inversion HCont. eauto. 
   (* mf *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
            intros HCont. inversion HCont.
   (* any *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
            intros HCont. inversion HCont.
   (* inter *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
            intros HCont. inversion HCont.
   (* class *) var_econ 2. apply LR_one.
            eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
            intros HCont. inversion HCont.
          (* not primval *)
          edestruct H5. edestruct H6. edestruct H8. var_econ 2. eapply LR_one. 
          eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        * inversion H4. inversion H0.
          (* is primval *)
          destruct t1. destruct (prim_ty_eq_dec pt p).
     (* == *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal. subst. auto.
     (* <> *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
              intros HCont. inversion HCont. eauto. 
     (* mf *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
              intros HCont. inversion HCont.
     (* any *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
              intros HCont. inversion HCont.
     (* inter *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
              intros HCont. inversion HCont.
     (* class *) var_econ 2. apply LR_one.
              eapply beta_E_contractAssign_onPrimVal_badType. unfold not. 
              intros HCont. inversion HCont.
            (* not primval *)
            edestruct H5. edestruct H6. edestruct H8. var_econ 2. eapply LR_one. 
            eapply beta_E_contractAssign_canReduce. eauto.
        + right. intros. edestruct H3; eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eauto.
        + right. intros. edestruct H3; eauto.
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto.
          * inversion H4. inversion H0.
            inversion H10.
            edestruct H10. edestruct H13. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H10.
            edestruct H10. edestruct H13. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto.
          * inversion H4. inversion H0.
            inversion H9.
            edestruct H9. edestruct H12. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H9.
            edestruct H9. edestruct H12. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto. 
          * inversion H4. inversion H0.
            inversion H6.
            edestruct H6. edestruct H9. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto. 
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.  
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.  
        + right. intros. edestruct H3; eauto. 
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.  
          * inversion H4. inversion H0.
            inversion H7.
            edestruct H7. edestruct H10. var_econ 2. eapply LR_one.
            eapply beta_E_contractAssign_canReduce. eapply beta_E_e_steps. eauto.  
      - intros. unfold methDefnIsValue. eauto.       
Admitted.    
    
 
 
(* preservation schtuff *)

(*Inductive appears_free_in_exp : var -> beta_exp -> Prop :=
  | afi_var : forall x,
              appears_free_in_exp x (beta_e_var x)
  | afi_theDefns : forall x md fd,
                   appears_free_in_mD x md ->
                   appears_free_in_fD x fd ->
                   appears_free_in_exp x (beta_e_theDefns md fd)
  | afi_newClass : forall x cn es,
                   appears_free_in_exp_list x es ->
                   appears_free_in_exp x (beta_e_newClass cn es)
  | afi_*)
  
  

  

  
  
  
  
  